from sympy import symbols, Eq, solve, cosh,sinh,log
import math
y,x,v,c1,c2=symbols('y x v c1 c2')

#https://mathworld.wolfram.com/PursuitCurve.html
#Jarrison's position is (0,vt). Our position is (x,y), with (x0,y0)=(50,0)
#Aadding a term v in the pursuit curve equations, our position is 
#the solution of the differential equation: [solve x*y''+sqrt(1+(y')^2)/v=0]
y=1/(v**2-1)*(v*x*(cosh(log(x)/v-c1)-v*sinh(log(x)/v-c1)))+c2
evaly=y.subs(x,50)-0
yeq=Eq(evaly,0)

#differentiate path equation: [d/dx 1/(v**2-1)*(v*x*(cosh(log(x)/v-c1)-v*sinh(log(x)/v-c1)))+c2]
yprime=sinh(c1)*cosh(log(x)/v)-cosh(c1)*sinh(log(x)/v)
evalyprime=yprime.subs(x,50)-0
yprimeeq=Eq(evalyprime,0)

#solve for constants with initial conditions y(50)=0, y'(50)=0
sol=solve((yeq,yprimeeq),(c1,c2))
ycomplete=y.subs(c1,sol[0][1]).subs(c2,sol[1][1])

#complete solution for path
print("equation of our path: y =", ycomplete)

#We must have that y(0)=100 for pursuer to reach the player
solvey=ycomplete.subs(x,0)-100
solveyEq=Eq(solvey,0)
solution=solve(solveyEq,v)
print(solution)

"""Output:
equation of our path: y = v*x*(-v*sinh(-50*v/(v**2 - 1) + log(x)/v) + cosh(-50*v/(v**2 - 1) + log(x)/v))/(v**2 - 1) - 50*v/(v**2 - 1)
[-1/4 + sqrt(17)/4, -sqrt(17)/4 - 1/4]

Jarrison is (sqrt(17)-1)/4, or 0.78078 times slower than us, so we are (1+sqrt(17))/4, or 1.28078 times faster than him.
Our speed is 1.28078*15 = 19.212 mph.

"""