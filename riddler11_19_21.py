import random, copy

cases=[[a,b,c,d] for a in range(3) for b in range(3) for c in range(3) for d in range(3)]
def evalstrat(ACchoice,BDchoice):
    numcorrect=0
    for case in cases:
        AC=ACchoice[(case[1],case[3])]
        BD=BDchoice[(case[0],case[2])]
        choices=[AC[0],BD[0],AC[1],BD[1]]
        if any(case[i]==choices[i] for i in range(4)):
            numcorrect+=1
    return numcorrect

ACchoice = {(case[1],case[3]) : [0,0] for case in cases}
BDchoice = {(case[0],case[2]) : [0,0] for case in cases}

init=evalstrat(ACchoice,BDchoice)
while init<81:
    ACnew=copy.deepcopy(ACchoice)
    BDnew=copy.deepcopy(BDchoice)
    ACcase=random.choice(list(ACchoice.keys()))
    BDcase=random.choice(list(BDchoice.keys()))
    ACnew[ACcase]=random.choices([0,1,2],k=2)
    BDnew[BDcase]=random.choices([0,1,2],k=2)
    newop=evalstrat(ACnew,BDnew)
    if newop>=init:
        init=newop
        ACchoice=ACnew
        BDchoice=BDnew
        #print(newop)
print("Solution for all 81 cases found:")

for ac in ACchoice:
    choices=ACchoice[ac]
    print("If players A and C see colors", ac ,"in players B and D, A guesses color", choices[0],"and C guesses color",choices[1])
print("")
for bd in BDchoice:
    choices=BDchoice[bd]
    print("If players B and D see colors", bd ,"in players A and C, B guesses color", choices[0],"and D guesses color",choices[1])


"""Sample output:
Solution for all 81 cases found:
If players A and C see colors (0, 0) in players B and D, A guesses color 2 and C guesses color 1
If players A and C see colors (0, 1) in players B and D, A guesses color 0 and C guesses color 1
If players A and C see colors (0, 2) in players B and D, A guesses color 2 and C guesses color 0
If players A and C see colors (1, 0) in players B and D, A guesses color 1 and C guesses color 2
If players A and C see colors (1, 1) in players B and D, A guesses color 1 and C guesses color 0
If players A and C see colors (1, 2) in players B and D, A guesses color 0 and C guesses color 2
If players A and C see colors (2, 0) in players B and D, A guesses color 0 and C guesses color 0
If players A and C see colors (2, 1) in players B and D, A guesses color 1 and C guesses color 1
If players A and C see colors (2, 2) in players B and D, A guesses color 2 and C guesses color 2
If players B and D see colors (0, 0) in players A and C, B guesses color 2 and D guesses color 0
If players B and D see colors (0, 1) in players A and C, B guesses color 1 and D guesses color 2
If players B and D see colors (0, 2) in players A and C, B guesses color 0 and D guesses color 1
If players B and D see colors (1, 0) in players A and C, B guesses color 0 and D guesses color 2
If players B and D see colors (1, 1) in players A and C, B guesses color 2 and D guesses color 2
If players B and D see colors (1, 2) in players A and C, B guesses color 0 and D guesses color 0
If players B and D see colors (2, 0) in players A and C, B guesses color 1 and D guesses color 1
If players B and D see colors (2, 1) in players A and C, B guesses color 1 and D guesses color 0
If players B and D see colors (2, 2) in players A and C, B guesses color 2 and D guesses color 1
"""
