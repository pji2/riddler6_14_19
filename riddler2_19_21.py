import random
def notfall(towers):
    towersr=towers[:]
    towersr.reverse()
    if len(towersr)<=2:
        return True
    for limit in range(2,len(towersr)):
        CM=sum(towersr[:limit])/limit
        centernextblock=towersr[limit]
         #print(limit,CM,centernextblock)
        if CM<centernextblock-0.5 or CM>centernextblock+0.5:
            #print("tower falls above block",len(towers)-limit,"from bottom")
            return False
    return True

ave=[]
for i in range(1000000):
    towers=[0]
    while notfall(towers):
        newblockcenter=towers[-1]+random.random()-0.5
        towers.append(newblockcenter)
        #print(towers)
    ave.append(len(towers))
print(sum(ave)/len(ave))