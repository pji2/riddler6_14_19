from __future__ import print_function
import sys
import collections

# Import Python wrapper for or-tools CP-SAT solver.
from ortools.sat.python import cp_model

def main(N):
    model = cp_model.CpModel()
    permutation=[0]
    for i in range(1,N):
        s = '_%i' % i
        var = model.NewIntVar(1,N-1,s)
        permutation.append(var)
    model.AddAllDifferent(permutation)
    for i in range(1,N):
        model.Add(permutation[i]!=i)
    for i in range(1,N):
        for j in range(1,N):
            if i!=j and permutation[j]>permutation[i]:
                model.Add((permutation[j]-permutation[i])!=(j-i)%N)

    solver = cp_model.CpSolver()
    status = solver.Solve(model)
    if status == cp_model.FEASIBLE:
        print(N," feasible")
        #for i in range(N):
        #    print(i,solver.Value(permutation[i]))
    else:
        print(N," not feasible")


if __name__ == '__main__':
    for n in range(3,15):
        main(n)
    
"""Output:
3  feasible
4  not feasible
5  feasible
6  not feasible
7  feasible
8  not feasible
9  feasible
10  not feasible
11  feasible
12  not feasible
13  feasible
14  not feasible
"""