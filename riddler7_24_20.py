import math
import matplotlib.pyplot as plt

class Dot:
    def __init__(self,x,y,slope):
        self.originx=x
        self.originy=y
        self.bounces=0
        self.x=x
        self.y=y
        self.slope=slope
        

def checkangle(u,v):
    mag_u=math.sqrt(u[0]**2+u[1]**2)
    mag_v=math.sqrt(v[0]**2+v[1]**2)
    dotproduct=u[0]*v[0]+u[1]*v[1]
    return math.acos(float(dotproduct)/(mag_u*mag_v))

def collides(dot):
    magnitude=math.sqrt(dot.x**2+dot.y**2)
    if magnitude>1 and dot.y<2:
        return False
    else:
        if dot.y>=2:
            #print("collide wall")
            wallnormal_x=0;wallnormal_y=-1
        else:
            #print("collide circle")
            wallnormal_x=dot.x/magnitude;wallnormal_y=dot.y/magnitude
        dot.bounces=dot.bounces+1
        dotvecx=math.cos(dot.slope);dotvecy=math.sin(dot.slope)
        angle_in=checkangle((dotvecx,dotvecy),(wallnormal_x,wallnormal_y))
        dotprod=dotvecx*wallnormal_x+dotvecy*wallnormal_y
        rx=dotvecx-2*dotprod*wallnormal_x
        ry=dotvecy-2*dotprod*wallnormal_y
        dot.slope=math.atan2(ry,rx)
        angle_out=checkangle((math.cos(dot.slope),math.sin(dot.slope)),(wallnormal_x,wallnormal_y))
        assert(abs(abs(180-angle_in*180/math.pi)-abs(angle_out*180/math.pi))<0.000001)
        #print(180-angle_in*180/math.pi,angle_out*180/math.pi)
        return True
        

def inbounds(dot):
    if dot.y<-1 or dot.x<-10000 or dot.x>10000:
        return False
    return True
    
def main(aimx):
    slope=math.atan2(2.0,(float(-aimx)+2))
    dot=Dot(-2,0,slope)
    dt=0
    axes = plt.gca()
    plt.axis('square')
    axes.set_xlim(-5, 5)
    axes.set_ylim(-1, 2)
    xdata=[];ydata=[]
    while inbounds(dot):
        dot.x=dot.originx+dt*math.cos(dot.slope)
        dot.y=dot.originy+dt*math.sin(dot.slope)
        dt+=.001
        if collides(dot):
            dot.originx=dot.x;dot.originy=dot.y
            dt=0.001
        xdata.append(dot.x)
        ydata.append(dot.y)
    #print(dot.bounces)
    if dot.bounces>=9:
        line, = axes.plot(xdata, ydata, 'r-')
        line.set_xdata(xdata)
        line.set_ydata(ydata)
        circle1 = plt.Circle((0, 0), 1, color='b', fill=False)
        axes.add_artist(circle1)
        plt.plot([-5,5], [2,2])
        plt.draw()
        plt.show()
        print(aimx,dot.bounces)
#for x in range(2000):
    #main(x/1000.0)
main(0.823)

"""
Output:
<Figure>
0.823 13
"""