import matplotlib.pyplot as plt
Goal=[10]
Turtle=[0]
Hare=[0,0,0,0]

goal=10
turtle=0
t=0
while turtle<goal:
    #turtle can reach goal before next expansion
    if turtle>goal-1:
        break
    turtle+=1
    t+=1
    turtle*=(t+1)/float(t)
    goal=10*(t+1)
    Turtle.append(turtle)
    Goal.append(goal)
if turtle<goal:
    t+=goal-turtle
    turtle=goal
print("turtle time, distance:",t,goal)

goal=10
hare=0
hstart=3.6666
T=math.ceil(hstart)
hare+=(T-hstart)*1.25 #distance traveled before 1st expansion
hare*=(T+1)/float(T)
goal=10*(T+1)
while hare<goal:
    #hare can reach goal before next expansion
    if hare>goal-1.25:
        break
    hare+=1.25
    T+=1
    hare*=(T+1)/float(T)
    goal=10*(T+1)
    Hare.append(hare)
if hare<goal:
    T+=(goal-hare)/1.25
    hare=goal
print("hare time, distance:",T,goal)
plt.plot(Goal)
plt.plot(Hare)
plt.plot(Turtle)
plt.show()

#Output:
#('turtle time, distance:', 12366.468116652977, 123670)
#('hare time, distance:', 12366.261999986262, 123670.0)