endgames=[] #all the possible ways the penalty kicks end. Assume teamB always kicks second

def sumdigits(n):
    return sum(int(x) for x in n)

def isendgame(Ascore,Bscore):
    a=sumdigits(Ascore)
    b=sumdigits(Bscore)
    br=5-len(Bscore)
    ar=5-len(Ascore)
    if a>b+br or b>a+ar:
        return True
    return False

def analyzewinpattern(pattern):
    playerA=pattern[:5]
    playerB=pattern[5:]
    Ascore=sumdigits(playerA)
    Bscore=sumdigits(playerB)
    tiedprob=0
    if Ascore==Bscore:
        tiedprob=.7**Ascore*.3**(5-Ascore)*.7**Bscore*.3**(5-Bscore)
        return tiedprob
    a_incr=0
    b_incr=0
    for turn in range(10):
        if turn%2==0:
            a_incr+=1
            pA=playerA[:a_incr]
            pB=playerB[:b_incr]
            res=isendgame(pA,pB)
        else:
            b_incr+=1
            pA=playerA[:a_incr]
            pB=playerB[:b_incr]
            res=isendgame(pA,pB)
        if res:
            if (pA,pB) not in endgames:
                endgames.append((pA,pB))
            break
        if turn==9:
            assert(sumdigits(pA)==sumdigits(pB))
    return 0
    

tiedprob=0
for n in range(1024):
    binary=str(bin(n))
    binary=binary[2:]
    binary='0'*(10-len(binary))+binary
    #print(binary)
    tied=analyzewinpattern(binary)
    tiedprob+=tied

Expectedshots=0
totalprob=0
for end in endgames:
    Apattern=end[0]
    Bpattern=end[1]
    Ascore=sumdigits(Apattern)
    Bscore=sumdigits(Bpattern)
    Amiss=len(Apattern)-Ascore
    Bmiss=len(Bpattern)-Bscore
    totalshots=len(Apattern)+len(Bpattern)
    prob=.7**(Ascore+Bscore)*.3**(Amiss+Bmiss)
    totalprob+=prob
    Expectedshots+=totalshots*prob
print(Expectedshots+tiedprob*(10+4.761904762))
print(len(endgames),"possible final scores:",endgames)

"""Output:
10.47014996193062
420 possible final scores: [('00000', '00001'), ('00000', '0001'), ('00000', '0010'), ('0000', '0011'), ('00000', '0100'), ('0000', '0101'), ('0000', '011'), ('00000', '1000'), ('0000', '1001'), ('0000', '101'), ('0000', '110'), ('000', '111'), ('00001', '00000'), ('00001', '00011'), ('00001', '00101'), ('00001', '01001'), ('00001', '10001'), ('00010', '00000'), ('00010', '00011'), ('00010', '00101'), ('00010', '0011'), ('00010', '01001'), ('00010', '0101'), ('00010', '0110'), ('0001', '0111'), ('00010', '10001'), ('00010', '1001'), ('00010', '1010'), ('0001', '1011'), ('00010', '1100'), ('0001', '1101'), ('00011', '0000'), ('00011', '00010'), ('00011', '00100'), ('00011', '00111'), ('00011', '01000'), ('00011', '01011'), ('00011', '01101'), ('00011', '10000'), ('00011', '10011'), ('00011', '10101'), ('00011', '11001'), ('00100', '00000'), ('00100', '00011'), ('00100', '00101'), ('00100', '0011'), ('00100', '01001'), ('00100', '0101'), ('00100', '0110'), ('0010', '0111'), ('00100', '10001'), ('00100', '1001'), ('00100', '1010'), ('0010', '1011'), ('00100', '1100'), ('0010', '1101'), ('0010', '111'), ('00101', '0000'), ('00101', '00010'), ('00101', '00100'), ('00101', '00111'), ('00101', '01000'), ('00101', '01011'), ('00101', '01101'), ('00101', '10000'), ('00101', '10011'), ('00101', '10101'), ('00101', '11001'), ('0011', '0000'), ('00110', '00010'), ('00110', '00100'), ('00110', '00111'), ('00110', '01000'), ('00110', '01011'), ('00110', '01101'), ('00110', '0111'), ('00110', '10000'), ('00110', '10011'), ('00110', '10101'), ('00110', '1011'), ('00110', '11001'), ('00110', '1101'), ('00110', '1110'), ('0011', '1111'), ('00111', '0001'), ('00111', '0010'), ('00111', '00110'), ('00111', '0100'), ('00111', '01010'), ('00111', '01100'), ('00111', '01111'), ('00111', '1000'), ('00111', '10010'), ('00111', '10100'), ('00111', '10111'), ('00111', '11000'), ('00111', '11011'), ('00111', '11101'), ('01000', '00000'), ('01000', '00011'), ('01000', '00101'), ('01000', '0011'), ('01000', '01001'), ('01000', '0101'), ('01000', '0110'), ('0100', '0111'), ('01000', '10001'), ('01000', '1001'), ('01000', '1010'), ('0100', '1011'), ('01000', '1100'), ('0100', '1101'), ('0100', '111'), ('01001', '0000'), ('01001', '00010'), ('01001', '00100'), ('01001', '00111'), ('01001', '01000'), ('01001', '01011'), ('01001', '01101'), ('01001', '10000'), ('01001', '10011'), ('01001', '10101'), ('01001', '11001'), ('0101', '0000'), ('01010', '00010'), ('01010', '00100'), ('01010', '00111'), ('01010', '01000'), ('01010', '01011'), ('01010', '01101'), ('01010', '0111'), ('01010', '10000'), ('01010', '10011'), ('01010', '10101'), ('01010', '1011'), ('01010', '11001'), ('01010', '1101'), ('01010', '1110'), ('0101', '1111'), ('01011', '0001'), ('01011', '0010'), ('01011', '00110'), ('01011', '0100'), ('01011', '01010'), ('01011', '01100'), ('01011', '01111'), ('01011', '1000'), ('01011', '10010'), ('01011', '10100'), ('01011', '10111'), ('01011', '11000'), ('01011', '11011'), ('01011', '11101'), ('0110', '0000'), ('01100', '00010'), ('01100', '00100'), ('01100', '00111'), ('01100', '01000'), ('01100', '01011'), ('01100', '01101'), ('01100', '0111'), ('01100', '10000'), ('01100', '10011'), ('01100', '10101'), ('01100', '1011'), ('01100', '11001'), ('01100', '1101'), ('01100', '1110'), ('0110', '1111'), ('01101', '0001'), ('01101', '0010'), ('01101', '00110'), ('01101', '0100'), ('01101', '01010'), ('01101', '01100'), ('01101', '01111'), ('01101', '1000'), ('01101', '10010'), ('01101', '10100'), ('01101', '10111'), ('01101', '11000'), ('01101', '11011'), ('01101', '11101'), ('0111', '000'), ('0111', '0010'), ('01110', '00110'), ('0111', '0100'), ('01110', '01010'), ('01110', '01100'), ('01110', '01111'), ('0111', '1000'), ('01110', '10010'), ('01110', '10100'), ('01110', '10111'), ('01110', '11000'), ('01110', '11011'), ('01110', '11101'), ('01110', '1111'), ('01111', '0011'), ('01111', '0101'), ('01111', '0110'), ('01111', '01110'), ('01111', '1001'), ('01111', '1010'), ('01111', '10110'), ('01111', '1100'), ('01111', '11010'), ('01111', '11100'), ('01111', '11111'), ('10000', '00000'), ('10000', '00011'), ('10000', '00101'), ('10000', '0011'), ('10000', '01001'), ('10000', '0101'), ('10000', '0110'), ('1000', '0111'), ('10000', '10001'), ('10000', '1001'), ('10000', '1010'), ('1000', '1011'), ('10000', '1100'), ('1000', '1101'), ('1000', '111'), ('10001', '0000'), ('10001', '00010'), ('10001', '00100'), ('10001', '00111'), ('10001', '01000'), ('10001', '01011'), ('10001', '01101'), ('10001', '10000'), ('10001', '10011'), ('10001', '10101'), ('10001', '11001'), ('1001', '0000'), ('10010', '00010'), ('10010', '00100'), ('10010', '00111'), ('10010', '01000'), ('10010', '01011'), ('10010', '01101'), ('10010', '0111'), ('10010', '10000'), ('10010', '10011'), ('10010', '10101'), ('10010', '1011'), ('10010', '11001'), ('10010', '1101'), ('10010', '1110'), ('1001', '1111'), ('10011', '0001'), ('10011', '0010'), ('10011', '00110'), ('10011', '0100'), ('10011', '01010'), ('10011', '01100'), ('10011', '01111'), ('10011', '1000'), ('10011', '10010'), ('10011', '10100'), ('10011', '10111'), ('10011', '11000'), ('10011', '11011'), ('10011', '11101'), ('1010', '0000'), ('10100', '00010'), ('10100', '00100'), ('10100', '00111'), ('10100', '01000'), ('10100', '01011'), ('10100', '01101'), ('10100', '0111'), ('10100', '10000'), ('10100', '10011'), ('10100', '10101'), ('10100', '1011'), ('10100', '11001'), ('10100', '1101'), ('10100', '1110'), ('1010', '1111'), ('10101', '0001'), ('10101', '0010'), ('10101', '00110'), ('10101', '0100'), ('10101', '01010'), ('10101', '01100'), ('10101', '01111'), ('10101', '1000'), ('10101', '10010'), ('10101', '10100'), ('10101', '10111'), ('10101', '11000'), ('10101', '11011'), ('10101', '11101'), ('1011', '000'), ('1011', '0010'), ('10110', '00110'), ('1011', '0100'), ('10110', '01010'), ('10110', '01100'), ('10110', '01111'), ('1011', '1000'), ('10110', '10010'), ('10110', '10100'), ('10110', '10111'), ('10110', '11000'), ('10110', '11011'), ('10110', '11101'), ('10110', '1111'), ('10111', '0011'), ('10111', '0101'), ('10111', '0110'), ('10111', '01110'), ('10111', '1001'), ('10111', '1010'), ('10111', '10110'), ('10111', '1100'), ('10111', '11010'), ('10111', '11100'), ('10111', '11111'), ('1100', '0000'), ('11000', '00010'), ('11000', '00100'), ('11000', '00111'), ('11000', '01000'), ('11000', '01011'), ('11000', '01101'), ('11000', '0111'), ('11000', '10000'), ('11000', '10011'), ('11000', '10101'), ('11000', '1011'), ('11000', '11001'), ('11000', '1101'), ('11000', '1110'), ('1100', '1111'), ('11001', '0001'), ('11001', '0010'), ('11001', '00110'), ('11001', '0100'), ('11001', '01010'), ('11001', '01100'), ('11001', '01111'), ('11001', '1000'), ('11001', '10010'), ('11001', '10100'), ('11001', '10111'), ('11001', '11000'), ('11001', '11011'), ('11001', '11101'), ('1101', '000'), ('1101', '0010'), ('11010', '00110'), ('1101', '0100'), ('11010', '01010'), ('11010', '01100'), ('11010', '01111'), ('1101', '1000'), ('11010', '10010'), ('11010', '10100'), ('11010', '10111'), ('11010', '11000'), ('11010', '11011'), ('11010', '11101'), ('11010', '1111'), ('11011', '0011'), ('11011', '0101'), ('11011', '0110'), ('11011', '01110'), ('11011', '1001'), ('11011', '1010'), ('11011', '10110'), ('11011', '1100'), ('11011', '11010'), ('11011', '11100'), ('11011', '11111'), ('111', '000'), ('1110', '0010'), ('11100', '00110'), ('1110', '0100'), ('11100', '01010'), ('11100', '01100'), ('11100', '01111'), ('1110', '1000'), ('11100', '10010'), ('11100', '10100'), ('11100', '10111'), ('11100', '11000'), ('11100', '11011'), ('11100', '11101'), ('11100', '1111'), ('11101', '0011'), ('11101', '0101'), ('11101', '0110'), ('11101', '01110'), ('11101', '1001'), ('11101', '1010'), ('11101', '10110'), ('11101', '1100'), ('11101', '11010'), ('11101', '11100'), ('11101', '11111'), ('1111', '001'), ('1111', '010'), ('1111', '0110'), ('11110', '01110'), ('1111', '100'), ('1111', '1010'), ('11110', '10110'), ('1111', '1100'), ('11110', '11010'), ('11110', '11100'), ('11110', '11111'), ('11111', '0111'), ('11111', '1011'), ('11111', '1101'), ('11111', '1110'), ('11111', '11110')]
"""