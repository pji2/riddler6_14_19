def mattobit(matrep):
    r=len(matrep)
    N=len(matrep[0])
    bitrep=""
    for i in range(r*N):
        bitrep+=str(matrep[int(i/N)][i%N])
    return bitrep

def bittomat(bitrep,N):
    matrep=[[0 for x in range(N)] for y in range(3)]
    for i in range(3*N):
        if bitrep[i]=='1':
            matrep[int(i/N)][i%N]=1
    return matrep

def analyzeseed(bitrep,N):
    statehist=[bitrep]
    mat=bittomat(bitrep,N)
    for gen in range(10):
        newmat=[m[:] for m in mat]
        for i in range(3):
            for j in range(N):
                total = mat[i][(j-1)%N] + mat[i][(j+1)%N] + mat[(i-1)%3][j] + mat[(i+1)%3][j] + \
                         mat[(i-1)%3][(j-1)%N] + mat[(i-1)%3][(j+1)%N] + \
                         mat[(i+1)%3][(j-1)%N] + mat[(i+1)%3][(j+1)%N]

                if mat[i][j]==1:
                    if total<2 or total>3:
                        newmat[i][j]=0
                else:
                    if total==3:
                        newmat[i][j]=1
        if mat==newmat:
            return ("notoscillator", 0)
        mat=newmat
        bits=mattobit(newmat)
        statehist.append(bits)
    if bitrep in statehist:
        return (statehist[-1], statehist.index(statehist[-1])-statehist.index(bitrep))
    return ("notoscillator", 0)
        
def findoscillations(N):
    oscillators=[]
    numstartstates=2**(3*N)
    for rep in range(numstartstates):
        strrep=bin(rep)[2:]
        infront=""
        if len(strrep)<3*N:
            infront+="0"*(3*N-len(strrep))
        strrep=infront+strrep
        result=analyzeseed(strrep,N)
        if result[0]!="notoscillator" and result[0] not in oscillators:
            oscillators.append(result[0])
    print(N," columns:" ,oscillators)
        
        
    


def main():
    for col in range(1,6):
        findoscillations(col)
        
main()
