#complete Python 3.6 code for riddler classic 1/3/2020

#step 1: narrow down word list to valid words
def validword(string):
    string=string.rstrip('\n')
    if 's' not in string and len(string)>=4 and len(set(string))<=7:
        return True
    else:
        return False
def preprocessfile():
    filepath='wordsraw.txt'
    fw=open('wordsvalid.txt',"w")
    with open(filepath) as fp:
        line = fp.readline()
        if validword(line):
            fw.write(line)
        while line:
            line = fp.readline()
            if validword(line):
                fw.write(line)
    fp.close()
    fw.close()
    
preprocessfile()

#step 2: get letter frequencies in the word list
def letterfreq(d):
    fo=open('wordsvalid.txt','r')
    line=fo.readline().rstrip('\n')
    for c in line:
        d[c]+=1
    while line:
        line=fo.readline().rstrip('\n')
        for c in line:
            d[c]+=1
    fo.close()
    print(d)
    
d={}
for l in 'abcdefghijklmnopqrstuvwxyz':
    d[l]=0
letterfreq(d)
print({k: v for k, v in sorted(d.items(), key=lambda item: item[1])})


#Step 3: Test various choices of letters for the honeycomb, where letters are among 
#the most frequently used letters
from itertools import combinations        
def points(string,letters,centralletter):
    inletters = set(string).issubset(set(letters))==True
    hascentral=0
    if(centralletter in set(string)):
        hascentral=1
    ispangram=0
    if(inletters and hascentral and len(set(string))==7):
        ispangram=1
    points=0
    if inletters and hascentral:
        if len(string)==4:
            points+=1
        else:
            points+=len(string)
    else:
        return [0,0]
    if ispangram:
        points+=7
        return [points,1]
    else:
        return [points,0]
    

fv=open('wordsvalid.txt',"r")
wordlist=fv.readlines()
fv.close()

#The action happens here
count=0
Centralletter='0'
maxpoints=0
maxletters=[]
for cl in ['e','a','i','r','n']:
    topletters="eairnlotdcgum"
    topletters = topletters.replace(cl,"")
    for item in combinations(topletters,6):
        haspangram=0;p=0
        count+=1
        letters=list(item)
        letters.append(cl)
        assert(len(set(letters))==7)
        for word in wordlist:
            word=word.rstrip('\n')
            ps=points(word,letters,cl)
            p+=ps[0]
            haspangram+=ps[1]
        if haspangram>0 and p>maxpoints:
            maxpoints=p
            maxletters=letters
            Centralletter=cl
        haspangram=0;p=0
print(maxpoints,maxletters,Centralletter)

#Output:

#{'a': 27615, 'b': 7636, 'c': 13621, 'd': 16461, 'e': 40678, 'f': 5417, 'g': 11812, 'h': 6909, 'i': 27032, 'j': 816, 'k': 4104, 'l': 20453, 'm': 9250, 'n': 23917, 'o': 20338, 'p': 9164, 'q': 544, 'r': 25813, 's': 0, 't': 20149, 'u': 10398, 'v': 3545, 'w': 3602, 'x': 1193, 'y': 6609, 'z': 1753}
#{'s': 0, 'q': 544, 'j': 816, 'x': 1193, 'z': 1753, 'v': 3545, 'w': 3602, 'k': 4104, 'f': 5417, 'y': 6609, 'h': 6909, 'b': 7636, 'p': 9164, 'm': 9250, 'u': 10398, 'g': 11812, 'c': 13621, 'd': 16461, 't': 20149, 'o': 20338, 'l': 20453, 'n': 23917, 'r': 25813, 'i': 27032, 'a': 27615, 'e': 40678}
#3898 ['e', 'a', 'i', 'n', 't', 'g', 'r'] r