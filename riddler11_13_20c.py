import operator as op
from functools import reduce

def ncr(n, r):
    #https://stackoverflow.com/questions/4941753/is-there-a-math-ncr-function-in-python
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer // denom  # or / in Python 2

#The probability that this state will lead to a win
def winprob(h,t):
    prob=0
    for i in range(51-h,101-h-t+1):
        prob+=ncr(101-h-t,i)*0.5**(101-h-t)
    return prob

#counts the number of paths to a losing state from a terminal (h,t) 99% state.
def countpaths(h,t,ninetyninestatesind):
    alls=[[0 for i in range(102)] for j in range(102)]
    for nn in ninetyninestatesind:
        if nn!=(h,t):
            alls[nn[0]][nn[1]]=-1
        else:
            alls[h][t]=1
    for totalflips in range(h+t+1,102):
        for h in range(0,totalflips+1):
            t=totalflips-h
            if alls[h][t]!=-1:
                alls[h][t]=max(alls[h-1][t],0)+max(alls[h][t-1],0)
    loseways=0
    for h in range(102):
        t=101-h
        if h<t:
            loseways+=alls[h][t]
    return loseways
    
#get all state with a 99% win prob
ninetyninestatesind=[]
for heads in range(0,52):
    for tails in range(0,101-heads+1):
        if heads+tails==101:
            continue
        else:
            if winprob(heads,tails)>=0.99:
                ninetyninestatesind.append((heads,tails))


#get all 99% states that lead to a non-99% state, and continue to a loss
totalloseways=0
for (h,t) in ninetyninestatesind:
    l=(h+1,t)
    r=(h,t+1)
    if (l[0]<102 and l not in ninetyninestatesind) or (r[1]<102 and r not in ninetyninestatesind):
        #print((h,t))
        ways=countpaths(h,t,ninetyninestatesind)
        totalloseways+=ncr(h+t,h)*ways
print(totalloseways/2**101)
print(totalloseways,"/",2**101)
    
#simulation
import random
ninetynineloss=0
for n in range(10000):
    totalflips=0
    h=0
    t=0
    ninetyninestate=0
    while totalflips<101:
        ht=random.randint(0,1)
        if ht==1:
            h+=1
        else:
            t+=1
        if h+t!=101 and winprob(h,t)>=0.99:
            ninetyninestate=1
        totalflips+=1
    assert(h+t==101)
    if h<t and ninetyninestate==1:
        ninetynineloss+=1
print(ninetynineloss)
        
#Output
#0.002117155741949852
#5367627494118744427158839101 / 2535301200456458802993406410752