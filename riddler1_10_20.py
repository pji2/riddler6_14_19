import num2words as nw

#get the score of a word by summing the score of its letters
def wordp(n):
    word=nw.num2words(n)
    points=0
    for s in word:
        if s in alphabet:
            points+=lettervalues[s]
    return points
alphabet="abcdefghijklmnopqrstuvwxyz"

#get the score of each letter, store in a dictionary
lettervalues=dict()
for i in range(len(alphabet)):
    lettervalues[alphabet[i]]=i+1

#tabulate the word score for the numbers 0-99, "hundred"
wordpoints=dict()
for n in range(1,100):
    wordpoints[n]=wordp(n)
wordpoints[100]=wordp(100)-wordpoints[1]
wordpoints[0]=0

#print(wordpoints)

#find the maximum number with a score greater than the value
def maxnumber(points, curr_num, maxnum, prefixbase):
    if prefixbase==10:
        for p in range(0,100):
            prev=points
            prev+=wordpoints[p]
            if curr_num+p<prev and curr_num+p>maxnum:
                maxnum=curr_num+p
    elif prefixbase==100:
        points=wordpoints[100]
        #100, 200.. 900
        for p in range(0,10):
            points+=wordpoints[p]
            if prefixbase*p<points and prefixbase*p>maxnum:
                maxnum=prefixbase*p
            maxnum = maxnumber(points, curr_num+prefixbase*p, maxnum, 10)
            points=wordpoints[100]
    return maxnum
print(maxnumber(-1000,0,0,100))
#Output
#279