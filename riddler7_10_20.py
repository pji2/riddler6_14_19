from itertools import combinations, permutations

def count(arr):
    c=0
    for i in arr:
        if i>0:
            c+=1
    return c

def placeblocks(blocks,n,order,N):
    for i in range(len(order)-1):
        spaces=n-(i+1)
        limitingblock=N-spaces+1
        o=order[i]
        if blocks[o-1]>=limitingblock:
            return 0
    return 1
    

def analyze(blocks,N):
    numvalid=0
    n=len(blocks)
    perms = permutations([x+1 for x in range(n)])
    for order in list(perms):
        p=placeblocks(blocks,n,order,N)
        numvalid+=p
    return numvalid

N=5
total=0
for numtaken in range(1,N+1):
    numvalid=0
    comb = combinations([x+1 for x in range(N)],numtaken)
    for selection in list(comb):
        num=analyze(selection,N)
        numvalid+=num
    total+=numvalid
print(total)
    