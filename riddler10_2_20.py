def P(m,d,s=0):
    if m>0 and d==0:
        return 1
    elif m==0 and d>0:
        return 0
    p_m=float(m)/(m+d)
    p_d=float(d)/(m+d)
    if s==0: #eat whatever chocolate you pick
        return p_m*P(m-1,d,1)+p_d*P(m,d-1,2)
    elif s==1: #must eat milk, otherwise resset
        return p_m*P(m-1,d,1)+p_d*P(m,d,0)
    elif s==2:
        return p_m*P(m,d,0)+p_d*P(m,d-1,2)
        
print(P(2,8))