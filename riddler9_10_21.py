import operator as op
from functools import reduce

def ncr(n, r):
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer // denom  # or / in Python 2

prob=0
E=0
for cuts in range(8,18):
    if cuts<10: #last cut is S
        p=ncr(cuts-1,7)*0.5**cuts
        prob+=p
        E+=p*cuts
    else:
        pL=ncr(cuts-1,9)*0.5**cuts
        pS=ncr(cuts-1,7)*0.5**cuts
        prob+=pL+pS
        E+=(pL+pS)*cuts
assert(prob==1)
print(E)

"""Output
14.29058837890625
"""