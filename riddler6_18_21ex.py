from itertools import combinations
total=0
for subset in combinations([x for x in range(10)],3):
    frames=[0 for x in range(12)]
    for s in subset:
        frames[s]=10
    score=0
    for f in range(10):
        if frames[f]==10:
            score+=frames[f]+frames[f+1]+frames[f+2]
    total+=score
print(total/120.0)

"""Output:
41.333333333333336
"""