paths=[
   #[1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12]
    [1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0],
    [1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0],
    [1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1],
    [1, 1, 0, 0, 1,-1,-1, 1, 0, 0, 1, 1],
    [1, 1, 0, 0, 1, 0,-1, 0, 1, 0, 0, 1],
    [1, 0, 0, 1, 0,-1, 0, 1, 0, 0, 1, 1],
    [0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1],
    [0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1],
    [0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0],
    [0, 1, 1,-1, 1, 0, 0, 1,-1, 1, 1, 0],
    [0, 0, 1, 0, 0, 0, 1, 1,-1, 1, 1, 0],
    [0, 1, 1,-1, 1, 1, 0, 0, 0, 1, 0, 0]
]

pathsreverse=[[-x for x in p] for p in paths]


def plancontainspath(plan,path):
    for i in range(len(path)):
        if path[i]==1 and plan[i]!=1:
            return False
        elif path[i]==-1 and plan[i]!=-1:
            return False
    return True

def haspath(plan,paths):
    for p in paths:
        if plancontainspath(plan,p):
            #print(plan,p)
            return True
    return False
   
def main(paths):
    count=0
    countreverse=0
    for road in range(2**12):
        arrangement=bin(road)[2:]
        arrangement='0'*(12-len(arrangement))+arrangement
        plan=[]
        for r in arrangement:
            if r=='0':
                plan.append(1)
            else:
                plan.append(-1)
        if haspath(plan,paths):
            count+=1
            if haspath(plan,pathsreverse):
                countreverse+=1
    print(count)
    print(count,"/",2**12)
    print(count/(2**12))
    print(countreverse)
main(paths)

"""
Output:
1135
1135 / 4096
0.277099609375
170
"""