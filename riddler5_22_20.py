states=["alabama",
"alaska",
"arizona",
"arkansas",
"california",
"colorado",
"connecticut",
"delaware",
"florida",
"georgia",
"hawaii",
"idaho",
"illinois",
"indiana",
"iowa",
"kansas",
"kentucky",
"louisiana",
"maine",
"maryland",
"massachusetts",
"michigan",
"minnesota",
"mississippi",
"missouri",
"montana",
"nebraska",
"nevada",
"new hampshire",
"new jersey",
"new mexico",
"new york",
"north carolina",
"north dakota",
"ohio",
"oklahoma",
"oregon",
"pennsylvania",
"rhode island",
"south carolina",
"south dakota",
"tennessee",
"texas",
"utah",
"vermont",
"virginia",
"washington",
"west virginia",
"wisconsin",
"wyoming"
]
alphabet=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']

alphadict={}
for a in alphabet:
    stateset=set()
    for s in states:
        if a in s:
            stateset.add(s)
    alphadict[a]=stateset

wordsfile = open("wordlist.txt",'r')
word = wordsfile.readline()
count = 0
wordswith1holdoutstate=[]
holdoutstates=[]
while word:
    word = wordsfile.readline()
    word = word[:len(word)-1]
    stateset = set()
    for char in word:
        stateset = stateset.union(alphadict[char])
    if len(stateset)==49:
        wordswith1holdoutstate.append(word)
        ho = set(states)-stateset
        #if word=='counterproductivenesses' or word=='hydrochlorofluorocarbon':
        #    print(ho)
        holdoutstates.append(ho.pop())
    count+=1
wordsfile.close()

#sort words from longest to shortest
wordswith1holdoutstate.sort(key=len, reverse=True)
print(wordswith1holdoutstate[:10])

#list holdout state by frequency
freqlist = sorted(holdoutstates, key=holdoutstates.count, reverse=True)
topstates=[]
for i in range(len(freqlist)):
    if len(topstates)==10:
        break
    elif freqlist[i] not in topstates:
        topstates.append(freqlist[i])
print(topstates)
#print(freqlist.count('ohio'))


#output:
#['counterproductivenesses', 'hydrochlorofluorocarbon', 'counterproductiveness', 'unconscientiousnesses', 'counterconditionings', 'deoxycorticosterones', 'expressionlessnesses', 'hyperconsciousnesses', 'hypersensitivenesses', 'incompressiblenesses']
#['ohio', 'alabama', 'utah', 'mississippi', 'hawaii', 'kentucky', 'wyoming', 'tennessee', 'alaska', 'nevada']