from ortools.sat.python import cp_model
#  abcde  <--carry digits
#   HAPPY
#HOLIDAYS
#HOHOHOHO

class VarArraySolutionPrinter(cp_model.CpSolverSolutionCallback):
    """Print intermediate solutions."""

    def __init__(self, variables):
        cp_model.CpSolverSolutionCallback.__init__(self)
        self.__variables = variables
        self.__solution_count = 0

    def on_solution_callback(self):
        self.__solution_count += 1
        for v in self.__variables:
            print('%s=%i' % (v, self.Value(v)), end=' ')
        print()

    def solution_count(self):
        return self.__solution_count
    
def ChannelingSampleSat():
    """Demonstrates how to link integer constraints together."""

    # Create the CP-SAT model.
    model = cp_model.CpModel()

    # Declare our two primary variables.
    H = model.NewIntVar(0, 9, 'H')
    A = model.NewIntVar(0, 9, 'A')
    P = model.NewIntVar(0, 9, 'P')
    Y = model.NewIntVar(0, 9, 'Y')
    O = model.NewIntVar(0, 9, 'O')
    L = model.NewIntVar(0, 9, 'L')
    I = model.NewIntVar(0, 9, 'I')
    D = model.NewIntVar(0, 9, 'D')
    S = model.NewIntVar(0, 9, 'S')
    a = model.NewIntVar(0, 1, 'a')
    b = model.NewIntVar(0, 1, 'b')
    c = model.NewIntVar(0, 1, 'c')
    d = model.NewIntVar(0, 1, 'd')
    e = model.NewIntVar(0, 1, 'e')
    
    acarry = model.NewBoolVar('a_c')
    bcarry = model.NewBoolVar('b_c')
    ccarry = model.NewBoolVar('c_c')
    dcarry = model.NewBoolVar('d_c')
    ecarry = model.NewBoolVar('e_c')
    
    model.Add(Y+S==O).OnlyEnforceIf(ecarry.Not())
    model.Add(Y+S-10==O).OnlyEnforceIf(ecarry)
    model.Add(e==0).OnlyEnforceIf(ecarry.Not())
    model.Add(e==1).OnlyEnforceIf(ecarry)
    
    model.Add(P+Y+e==H).OnlyEnforceIf(dcarry.Not())
    model.Add(P+Y+e-10==H).OnlyEnforceIf(dcarry)
    model.Add(d==0).OnlyEnforceIf(dcarry.Not())
    model.Add(d==1).OnlyEnforceIf(dcarry)
    
    model.Add(P+A+d==O).OnlyEnforceIf(ccarry.Not())
    model.Add(P+A+d-10==O).OnlyEnforceIf(ccarry)
    model.Add(c==0).OnlyEnforceIf(ccarry.Not())
    model.Add(c==1).OnlyEnforceIf(ccarry)
    
    model.Add(A+D+c==H).OnlyEnforceIf(bcarry.Not())
    model.Add(A+D+c-10==H).OnlyEnforceIf(bcarry)
    model.Add(b==0).OnlyEnforceIf(bcarry.Not())
    model.Add(b==1).OnlyEnforceIf(bcarry)
    
    model.Add(H+I+b==O).OnlyEnforceIf(acarry.Not())
    model.Add(H+I+b-10==O).OnlyEnforceIf(acarry)
    model.Add(a==0).OnlyEnforceIf(acarry.Not())
    model.Add(a==1).OnlyEnforceIf(acarry)
    
    model.Add(L+a==H)
    model.AddAllDifferent([H,A,P,Y,O,L,I,D,S])
    
    solver = cp_model.CpSolver()
    # Enumerate all solutions.
    solver.parameters.enumerate_all_solutions = True

    # Search and print out all solutions.
    solution_printer = VarArraySolutionPrinter([H,A,P,Y,O,L,I,D,S,a,b,c,d,e])
    status=solver.SearchForAllSolutions(model, solution_printer)
ChannelingSampleSat()

"""Output:
H=6 A=8 P=3 Y=2 O=1 L=5 I=4 D=7 S=9 a=1 b=1 c=1 d=0 e=1 
H=8 A=4 P=6 Y=1 O=0 L=7 I=2 D=3 S=9 a=1 b=0 c=1 d=0 e=1 
"""