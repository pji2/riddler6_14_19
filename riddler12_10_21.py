import operator as op
from functools import reduce

def ncr(n, r):
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer // denom  # or / in Python 2
    
tp=0
winps=[.25,.5,.75]
threshold=15
def prob(roundnum,winner,player0,player1,currentprob):
    if roundnum==0:
        if winner==0:
            p0=15
            for p1 in range(15):
                newp=ncr(p0-1+p1,p1)*winps[0]**p0*(1-winps[0])**p1
                #print(p0,p1,newp)
                prob(1,0,p0,p1,newp)
                prob(1,1,p0,p1,newp)
        if winner==1:
            p1=15
            for p0 in range(15):
                newp=ncr(p0+p1-1,p0)*winps[0]**p0*(1-winps[0])**p1
                prob(1,0,p0,p1,newp)
                prob(1,1,p0,p1,newp)
    if roundnum==1:
        #print("r1",winner,player0,player1,currentprob)
        if winner==0:
            p0=30-player0
            for p1 in range(30-player1):
                newp=ncr(p0-1+p1,p1)*winps[1]**p0*(1-winps[1])**p1
                prob(2,0,player0+p0,player1+p1,currentprob*newp)
                prob(2,1,player0+p0,player1+p1,currentprob*newp)
        if winner==1:
            p1=30-player1
            for p0 in range(30-player0):
                newp=ncr(p0+p1-1,p0)*winps[1]**p0*(1-winps[1])**p1
                prob(2,0,player0+p0,player1+p1,currentprob*newp)
                prob(2,1,player0+p0,player1+p1,currentprob*newp)
    if roundnum==2:
        #print("r2",player0,player1,currentprob)
        if winner==0:
            p0=45-player0
            for p1 in range(45-player1):
                newp=ncr(p0-1+p1,p1)*winps[2]**p0*(1-winps[2])**p1
                global tp
                tp+=currentprob*newp
        if winner==1:
            p1=45-player1
            for p0 in range(45-player0):
                newp=ncr(p0+p1-1,p0)*winps[2]**p0*(1-winps[2])**p1
                #check if sum of probabilities is 1
                #tp+=currentprob*newp
prob(0,1,0,0,0)
prob(0,0,0,0,0)
print(tp)

"""Output:
0.9317336077289655
"""