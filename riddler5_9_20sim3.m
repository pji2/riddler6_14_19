%558 95% interval: [542-574]
hist=[];
ntrials=100;
numsensors=100000;
for t=1:ntrials
N=0;
sensors=numsensors;
eval=rand(sensors,2);
pointseval=zeros(sensors,2);
pointseval(:,1)=2*pi*eval(:,1);
pointseval(:,2)=acos(2*eval(:,2)-1);
deleted=0;
while sensors>0
    N=N+1;
    thetapick=2*pi*rand();
    phipick=acos(2*rand()-1);
    deletedsensors=[];
    for index=1:sensors
        theta=pointseval(index,1);
        phi=pointseval(index,2);
        dist=4*acos(cos(phi)*cos(phipick)+sin(phi)*sin(phipick)*cos(abs(theta-thetapick)));
        if dist<=1
            deletedsensors=[deletedsensors,index];
        end
    end
    deletedsensors=unique(deletedsensors);
    pointseval(deletedsensors,:)=[];
    deleted=deleted+length(deletedsensors);
    sensors=sensors-length(deletedsensors);
end
assert(sensors>=0)
assert(deleted==numsensors)
hist=[hist N];
end
average=mean(hist)
histogram(hist,30)
