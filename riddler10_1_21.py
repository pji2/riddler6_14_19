"""Output:
solved for weeks= 12
For week: 0 Player 0 at South lookout
For week: 0 Player 1 at South lookout
For week: 0 Player 2 at North lookout
For week: 0 Player 3 at North lookout
For week: 1 Player 0 at South lookout
For week: 1 Player 1 at North lookout
For week: 1 Player 2 at South lookout
For week: 1 Player 3 at North lookout
For week: 2 Player 0 at North lookout
For week: 2 Player 1 at North lookout
For week: 2 Player 2 at South lookout
For week: 2 Player 3 at South lookout
For week: 3 Player 0 at North lookout
For week: 3 Player 1 at South lookout
For week: 3 Player 2 at South lookout
For week: 3 Player 3 at North lookout
For week: 4 Player 0 at North lookout
For week: 4 Player 1 at South lookout
For week: 4 Player 2 at North lookout
For week: 4 Player 3 at South lookout
For week: 5 Player 0 at South lookout
For week: 5 Player 1 at North lookout
For week: 5 Player 2 at North lookout
For week: 5 Player 3 at South lookout
For week: 6 Player 0 at South lookout
For week: 6 Player 1 at South lookout
For week: 6 Player 2 at North lookout
For week: 6 Player 3 at North lookout
For week: 7 Player 0 at North lookout
For week: 7 Player 1 at South lookout
For week: 7 Player 2 at South lookout
For week: 7 Player 3 at North lookout
For week: 8 Player 0 at South lookout
For week: 8 Player 1 at North lookout
For week: 8 Player 2 at South lookout
For week: 8 Player 3 at North lookout
For week: 9 Player 0 at South lookout
For week: 9 Player 1 at North lookout
For week: 9 Player 2 at North lookout
For week: 9 Player 3 at South lookout
For week: 10 Player 0 at North lookout
For week: 10 Player 1 at North lookout
For week: 10 Player 2 at South lookout
For week: 10 Player 3 at South lookout
For week: 11 Player 0 at North lookout
For week: 11 Player 1 at South lookout
For week: 11 Player 2 at North lookout
For week: 11 Player 3 at South lookout
"""
#Program:
from ortools.sat.python import cp_model
from itertools import combinations

def main(W):
    NumRangers=4
    NumWeeks=W
    
    model = cp_model.CpModel()
    assignments={}
    for r in range(NumRangers):
        for w in range(NumWeeks):
            for l in range(2):
                assignments[(r,w,l)]=model.NewIntVar(0,1,'assign%ir%iw%il' % (r,w,l))
                
    switchesSlack={}
    switches={}
    for r in range(NumRangers):
        for w in range(NumWeeks):
            switchesSlack[(r,w)]=model.NewBoolVar('flag%ir%iw'%(r,w))
            switches[(r,w)]=model.NewIntVar(0,1,'switch%ir%iw' % (r,w))
    
    #each ranger spends same number of weeks paired with each other ranger
    pairings={}
    pairingsSlack={}
    comb=combinations([x for x in range(NumRangers)],2)
    combos=list(comb)
    for c in combos:
        for w in range(NumWeeks):
            pairings[(w,c[0],c[1])]=model.NewIntVar(0,1,'pairing%iw%ia%ib'%(w,c[0],c[1]))
            pairingsSlack[(w,c[0],c[1])]=model.NewBoolVar('pflag%iw%ia%ib'%(w,c[0],c[1]))
    comb=combinations([x for x in range(NumRangers)],2)
    for c in list(comb):
        for w in range(NumWeeks):
            model.Add(assignments[(c[0],w,0)]==assignments[(c[1],w,0)]).OnlyEnforceIf(pairingsSlack[(w,c[0],c[1])])
            model.Add(pairings[(w,c[0],c[1])]==1).OnlyEnforceIf(pairingsSlack[(w,c[0],c[1])])
            model.Add(assignments[(c[0],w,0)]!=assignments[(c[1],w,0)]).OnlyEnforceIf(pairingsSlack[(w,c[0],c[1])].Not())
            model.Add(pairings[(w,c[0],c[1])]==0).OnlyEnforceIf(pairingsSlack[(w,c[0],c[1])].Not())

    for c in combos[1:]:
        model.Add(sum(pairings[(w,0,1)] for w in range(NumWeeks))==sum(pairings[(w,c[0],c[1])] for w in range(NumWeeks)))
    
    #each ranger has 1 assignment per week
    for r in range(NumRangers):            
        for w in range(NumWeeks):
            assignsperweek=assignments[(r,w,0)]+assignments[(r,w,1)]
            model.Add(assignsperweek==1)
            
    #2 rangers per North/South
    for w in range(NumWeeks):
        rangersNorth=0
        rangersSouth=0
        for r in range(NumRangers):
            rangersNorth+=assignments[(r,w,0)]
            rangersSouth+=assignments[(r,w,1)]
        model.Add(rangersNorth==2)
        model.Add(rangersSouth==2)
        
    #each ranger spends same number of weeks N/S
    for r in range(NumRangers):
        weeksN=0
        weeksS=0
        for w in range(NumWeeks):
            weeksN+=assignments[(r,w,0)]
            weeksS+=assignments[(r,w,1)]
        model.Add(weeksN==weeksS)
    

    #2 rangers switch every week
    for w in range(NumWeeks):
        for r in range(NumRangers):
            model.Add(switches[(r,w)]==1).OnlyEnforceIf(switchesSlack[(r,w)])
            model.Add(switches[(r,w)]==0).OnlyEnforceIf(switchesSlack[(r,w)].Not())
            model.Add(assignments[(r,w,0)]!=assignments[(r,(w+1)%NumWeeks,0)]).OnlyEnforceIf(switchesSlack[(r,w)])
            model.Add(assignments[(r,w,0)]==assignments[(r,(w+1)%NumWeeks,0)]).OnlyEnforceIf(switchesSlack[(r,w)].Not())
            
    for w in range(NumWeeks):
        model.Add(sum(switches[(r,w)] for r in range(NumRangers))==2)
        
    #each ranger switches same number of times
    switchperR=[]
    for r in range(NumRangers):
        switchperR.append(sum(switches[(r,w)] for w in range(NumWeeks)))
    for r in range(NumRangers-1):
        model.Add(switchperR[r]==switchperR[r+1])
        
    solver = cp_model.CpSolver()
    status=solver.Solve(model)
    if status==cp_model.FEASIBLE or status==cp_model.OPTIMAL:
	print("solved for weeks=",W)
        for w in range(NumWeeks):
            for r in range(NumRangers):
                if solver.Value(assignments[(r,w,0)])==1:
                    print("For week:", w, "Player", r,"at North lookout")
                elif solver.Value(assignments[(r,w,1)])==1:
                    print("For week:", w, "Player", r,"at South lookout")
        """for w in range(NumWeeks):
            for r in range(NumRangers):
                if solver.Value(switches[(r,w)])==1:
                    print("switched:",w,r)"""
for n in range(2,20):
    main(n)
