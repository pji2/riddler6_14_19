#volume of tetrahedron using Cayley-Menger determinant
import numpy as np
import sympy as sp
from sympy import *
from sympy.matrices import Matrix
d12,d13,d34=sp.symbols('d12,d13,d34')
M=np.array([[0,1,1,1,1],[1,0,d12**2,d13**2,1],[1,d12**2,0,1,1],[1,d13**2,1,0,d34**2],[1,1,1,d34**2,0]])
S=sp.Matrix(M)
Eq=S.det()

#five sides are the same
Eq5sides=Eq.subs(d13,1).subs(d34,1)
eqdiff=sp.diff(Eq5sides,d12)
eqdiff1=sp.Eq(eqdiff,0)
print("possible side lengths:",sp.solve(eqdiff1,d12))
print(sp.sqrt((Eq5sides.subs(d12,sp.sqrt(6)/2))/288))
#max Volume for 5 sides=1 is 1/8. Eq5sides=9/2

#the two different sides share a vertex
Eq4=Eq.subs(d34,1)
Eq4diff_1=sp.diff(Eq4,d12)
Eq4diff_2=sp.diff(Eq4,d13)
eq1=sp.Eq(Eq4diff_1,0)
eq2=sp.Eq(Eq4diff_2,0)
solv=sp.solve((eq1,eq2),(d12,d13))
print("possible sides lengths:",solv)
#Eq4=6, d12=d13=sqrt(2), volume = sqrt(3)/12 = .1443
print(sp.sqrt((Eq4.subs(d12,sp.sqrt(2)).subs(d13,sp.sqrt(2)))/288))
#max volume for 4 sides=1 is volume = sqrt(3)/12 = .1443, d12=d13=sqrt(2)

#two opposite sides are different
Eq4=Eq.subs(d13,1)
Eq4diff_1=sp.diff(Eq4,d12)
Eq4diff_2=sp.diff(Eq4,d34)
eq1=sp.Eq(Eq4diff_1,0)
eq2=sp.Eq(Eq4diff_2,0)
solv=sp.solve((eq1,eq2),(d12,d34))
print("possible side lengths:",solv)
print(sp.sqrt((Eq4.subs(d12,2*sp.sqrt(3)/3).subs(d34,2*sp.sqrt(3)/3))/288))
#max volume for 4 sides=1 is volume=2/27*sqrt(3) = .1283, d12=d34=2/sqrt(3)

###Output:

possible side lengths: [0, -sqrt(6)/2, sqrt(6)/2]
1/8
possible sides lengths: [(-1, 0), (0, -1), (0, 0), (0, 1), (1, 0), (-sqrt(2), -sqrt(2)), (-sqrt(2), sqrt(2)), (sqrt(2), -sqrt(2)), (sqrt(2), sqrt(2))]
sqrt(3)/12
possible side lengths: [(0, -2), (0, -2), (0, 2), (0, 2), (0, d34), (-sqrt(2), 0), (sqrt(2), 0), (-2*sqrt(3)/3, -2*sqrt(3)/3), (-2*sqrt(3)/3, 2*sqrt(3)/3), (2*sqrt(3)/3, -2*sqrt(3)/3), (2*sqrt(3)/3, 2*sqrt(3)/3), (d12, 0)]
2*sqrt(3)/27

"""