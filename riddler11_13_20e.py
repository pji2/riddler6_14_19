import operator as op
from functools import reduce
from math import factorial

def ncr(n, r):
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer // denom  # or / in Python 2

#riddler express
jeopardygrid=[[200 for i in range(6)],[400 for j in range(6)],[600 for j in range(6)],[800 for j in range(6)],[1000 for j in range(6)]]
flattenarr=[i for s in jeopardygrid for i in s]
avewinnings=0
for dd in range(30):
    currentwinnings=sum(flattenarr[:dd])
    if currentwinnings<1000:
        currentwinnings+=1000
    else:
        currentwinnings*=2
    currentwinnings+=sum(flattenarr[dd+1:])
    avewinnings+=currentwinnings/float(30)
print(avewinnings)

#EC
numcases=0
total=0
for dd in range(30):
    arange=6;brange=6;crange=6;drange=6;erange=6
    if dd>=0 and dd<=5:
        arange=5
    if dd>=6 and dd<=11:
        brange=5
    if dd>=12 and dd<=17:
        crange=5
    if dd>=18 and dd<=23:
        drange=5
    if dd>=24 and dd<=29:
        erange=5
    totalpossiblewinnings=arange*200+brange*400+crange*600+drange*800+erange*1000
    for a in range(arange+1):
        for b in range(brange+1):
            for c in range(crange+1):
                for d in range(drange+1):
                    for e in range(erange+1):
                        currentwinnings=a*200+b*400+c*600+d*800+e*1000
                        afterddwinnings=totalpossiblewinnings-currentwinnings
                        if currentwinnings<1000:
                            currentwinnings+=1000
                        else:
                            currentwinnings*=2
                        currentwinnings+=afterddwinnings
			#first pick how many of each 200, 400, ... you want, then account for all possible orderings
                        #of the pre-Daily double numbers and the post-Daily double numbers
                        currentwinnings*=ncr(arange,a)*ncr(brange,b)*ncr(crange,c)*ncr(drange,d)*ncr(erange,e)*\
                        factorial(a+b+c+d+e)*factorial(29-a-b-c-d-e)
                        total+=currentwinnings
                        numcases+=ncr(arange,a)*ncr(brange,b)*ncr(crange,c)*ncr(drange,d)*ncr(erange,e)*\
                        factorial(a+b+c+d+e)*factorial(29-a-b-c-d-e)
print(total/float(numcases))
print(total,"/",numcases)

#Output
#23800.000000000004
#26149.458492975733
#208086559434060039944288914636800000000 / 7957585794365731759089254400000000