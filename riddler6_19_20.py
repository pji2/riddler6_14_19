# Import Python wrapper for or-tools CP-SAT solver.
from ortools.sat.python import cp_model

def findSpheres(N,C):
    spheres=[n+1 for n in range(N)]
    children=[c for c in range(1,C+1)]
    spherevols=[i**3 for i in spheres]
    #print(spherevols)
    totalvol=sum(spherevols)
    if int(totalvol/C)-totalvol/C != 0:
        return 0
    model=cp_model.CpModel()
    choices={}
    for n in spheres:
        for p in children:
            label = str(n)+"/"+str(p)
            choices[n,p]=model.NewIntVar(0,1,label)
    
    #each child has an equal volume
    for p in children:
        model.Add(sum(choices[n,p]*int(spherevols[n-1]) for n in spheres)==int(totalvol/3))
    
    #each sphere picked once
    for n in spheres:
        model.Add(sum(choices[n,p] for p in children)==1)
        
    #solve model
    solver = cp_model.CpSolver()
    
    status = solver.Solve(model)
    #print(status)
    if status==cp_model.FEASIBLE:
        subsets=[[] for x in range(C)]
        for p in children:
            for n in spheres:
                if solver.Value(choices[n,p])==1:
                    subsets[p-1].append(n)
        print("sets for each children are:", subsets)
    return status 

for spheres in range(1,50):
    status=findSpheres(spheres,3)
    if status==0:
        print(spheres, "spheres, total volume not divisible by 3")
    elif status==1 or status==2:
        print("solution found for", spheres, "spheres")
        break

"""
Output:
1 spheres, total volume not divisible by 3
4 spheres, total volume not divisible by 3
7 spheres, total volume not divisible by 3
10 spheres, total volume not divisible by 3
13 spheres, total volume not divisible by 3
16 spheres, total volume not divisible by 3
19 spheres, total volume not divisible by 3
22 spheres, total volume not divisible by 3
sets for each children are: [[3, 6, 10, 13, 18, 19, 21], [2, 5, 9, 11, 14, 15, 17, 23], [1, 4, 7, 8, 12, 16, 20, 22]]
solution found for 23 spheres
"""