from __future__ import print_function

from ortools.sat.python import cp_model

import csv

def BinpackingProblemSat():
    """Solves a bin-packing problem using the CP-SAT solver."""
    # Data.
    ballots=400
    players=20

    # Model.
    model = cp_model.CpModel()

    # Main variables.
    x = {}
    for b in range(ballots):
        for p in range(players):
            x[(b, p)] = model.NewIntVar(0, 1, 'x_%i_%i' % (b, p))

    # Is Player Inducted? variables.
    inducted = [model.NewBoolVar('inducted_%i' % p) for p in range(players)]    
    
    # Number of votes each player received
    votes_per_player = [model.NewIntVar(0,ballots,'votes_%i' %p) for p in range(players)]

    # Links votes_per_player and x
    for p in range(players):
        model.Add(votes_per_player[p] == sum(x[(b, p)] for b in range(ballots)))

    #Constraint 1
    for b in range(ballots):
        model.Add(sum(x[(b,p)] for p in range(players))==10)
        
    # Links inducted and votes_per_player through an equivalence relation.
    requiredcapacity=300
    for p in range(players):
        # player has at least 300 votes if inducted
        model.Add(votes_per_player[p] >= requiredcapacity).OnlyEnforceIf(inducted[p])
        # less than 300 votes if not inducted
        model.Add(votes_per_player[p] < requiredcapacity).OnlyEnforceIf(inducted[p].Not())

    # Maximize number of inducted players
    model.Maximize(sum(inducted))

    # Solves and prints out the solution.
    solver = cp_model.CpSolver()
    status = solver.Solve(model)
    print('Solve status: %s' % solver.StatusName(status))
    if status == cp_model.OPTIMAL:
        print('Optimal objective value (Max number of players inducted) : %i' % solver.ObjectiveValue())
    print('Statistics')
    print('  - conflicts : %i' % solver.NumConflicts())
    print('  - branches  : %i' % solver.NumBranches())
    print('  - wall time : %f s' % solver.WallTime())
    
    #prints out ballot matrix for every player. Matrix value is 1 if player p was selected by ballot b
    f = open('riddler1_24_20e_output.csv','w')
    for p in range(players):
        arr="Player %i: ," % (p+1)
        for b in range(ballots):
            arr+=str(solver.Value(x[(b,p)]))+","
        #print('Player %i: ' % (p+1) , arr)
        f.write(arr+"\n")
    print("Wrote possible vote distribution to file")
    f.close()
    

BinpackingProblemSat()
#validated in riddler1_24_20validation.csv!
#Partial output:

"""
Solve status: OPTIMAL
Optimal objective value (Max number of players inducted) : 13
Statistics
  - conflicts : 0
  - branches  : 23167
  - wall time : 1.100773 s
"""