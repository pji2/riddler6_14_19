from itertools import combinations
from ortools.sat.python import cp_model
import numpy as np

def identifyIsocelesSets(N):
    points=[x for x in range(N*N)]
    isocelessets=[]
    for trianglecheck in combinations(points,3):
        pa=trianglecheck[0];pb=trianglecheck[1];pc=trianglecheck[2]
        sidelengths=[]
        pa_xy=(int(pa/N),pa%N)
        pb_xy=(int(pb/N),pb%N)
        pc_xy=(int(pc/N),pc%N)
        sidelengths.append((pa_xy[0]-pb_xy[0])**2+(pa_xy[1]-pb_xy[1])**2)
        sidelengths.append((pc_xy[0]-pb_xy[0])**2+(pc_xy[1]-pb_xy[1])**2)
        sidelengths.append((pa_xy[0]-pc_xy[0])**2+(pa_xy[1]-pc_xy[1])**2)
        if len(set(sidelengths))!=len(sidelengths):
            isocelessets.append((pa,pb,pc))
            
    point_tri_matrix=[[0 for x in range(len(isocelessets))] for y in range(N*N)]
    for t in range(len(isocelessets)):
        set_=isocelessets[t]
        point_tri_matrix[set_[0]][t]=1
        point_tri_matrix[set_[1]][t]=1
        point_tri_matrix[set_[2]][t]=1
    return point_tri_matrix

def model(N,isocelesset):
    model = cp_model.CpModel()
    points=[]
    for i in range(N*N):
        pi=model.NewIntVar(0, 1, "point"+str(i))
        points.append(pi)

    result=np.matmul(points,isocelesset)

    for i in range(len(isocelesset[0])):
        model.Add(result[i]<3)

    model.Maximize(sum(points))
    solver = cp_model.CpSolver()
    status = solver.Solve(model)
    if status == cp_model.OPTIMAL:
        for i in range(N*N):
            print(solver.Value(points[i]))

N=4
matrix=identifyIsocelesSets(N)
model(N,matrix)
"""Output:
1
0
1
0
1
0
0
1
0
0
0
0
0
0
1
1
"""
#N=4, 6
#N=5, 7
#N=6, 9
#N=7, 10