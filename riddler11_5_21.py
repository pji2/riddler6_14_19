import math
def get_prime_factorization(number):
    #https://paulrohan.medium.com/prime-factorization-of-a-number-in-python-and-why-we-check-upto-the-square-root-of-the-number-111de56541f
    
    # create an empty list and later I will
    # run a for loop with range() function using the append() method to add elements to the list.
    prime_factors = []

    # First get the number of two's that divide number
    # i.e the number of 2's that are in the factors
    while number % 2 == 0:
        prime_factors.append(2)
        number = number / 2

    # After the above while loop, when number has been
    # divided by all the 2's - so the number must be odd at this point
    # Otherwise it would be perfectly divisible by 2 another time
    # so now that its odd I can skip 2 ( i = i + 2) for each increment
    for i in range(3, int(math.sqrt(number)) + 1, 2):
        while number % i == 0:
            prime_factors.append(int(i))
            number = number / i


    if number > 2:
        prime_factors.append(int(number))

    return prime_factors

def getsumfactors(N):
    factorization=get_prime_factorization(N)
    uniquefactors=list(set(factorization))
    count_uniques=[]
    for f in uniquefactors:
        count_uniques.append(factorization.count(f))
    sumfactors=1
    for i in range(len(uniquefactors)):
        x=uniquefactors[i]
        a=count_uniques[i]
        sumfactors*=(x**(a+1)-1)/(x-1)
    return sumfactors

for x in range(36,1000000):
    if getsumfactors(x)==round(x*2.54):
        print(x)

"""Output:
36
378
49600
"""