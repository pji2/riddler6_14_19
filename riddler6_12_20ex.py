def bestchoicefor2nd(val,rolls):
    best=None
    points=-10
    for option2 in rolls:
        p=0
        for v in option2:
            s=val+v
            if s==7 or s==11:
                p+=1
            elif s==2 or s==3 or s==12:
                p-=1
        if p>points:
            best=option2
            points=p
    return best,points

rolls=[(2,3,4,5),(1,3,4,6),(1,2,5,6)]
optimal1=-10
best=None
for option1 in rolls:
    p=0
    for val in option1:
        opt2, points=bestchoicefor2nd(val,rolls)
        p+=points
    if p>optimal1:
        optimal1=p
        best=option1
print(optimal1,best)

for i in range(1,7):
    s,p=bestchoicefor2nd(i,rolls)
    print("if you roll a ", i, "then you roll the set", s, "for expected", p, "/4 points")

#Output
#(5, (2, 3, 4, 5))
#('if you roll a ', 1, 'then you roll the set', (1, 3, 4, 6), 'for expected', 0, '/4 points')
#('if you roll a ', 2, 'then you roll the set', (2, 3, 4, 5), 'for expected', 1, '/4 points')
#('if you roll a ', 3, 'then you roll the set', (2, 3, 4, 5), 'for expected', 1, '/4 points')
#('if you roll a ', 4, 'then you roll the set', (2, 3, 4, 5), 'for expected', 1, '/4 points')
#('if you roll a ', 5, 'then you roll the set', (1, 2, 5, 6), 'for expected', 2, '/4 points')
#('if you roll a ', 6, 'then you roll the set', (2, 3, 4, 5), 'for expected', 1, '/4 points')

#5/16 points
