#Riddler Express 11-12-21
def probIncreasingSeq(dpmax):
    Ndice=len(dpmax)
    maxdice=max(dpmax)
    dp=[[0 for x in range(maxdice)] for y in range(Ndice)]
    for i in range(dpmax[0]):
        dp[0][i]=1
    for dicenum in range(1,Ndice):
        for num in range(dpmax[dicenum]):
            sum_=0
            for i in range(num):
                sum_+=dp[dicenum-1][i]
            dp[dicenum][num]=sum_
            #print(dicenum,num,sum_)
    NumIncreasingSeq=sum([dp[Ndice-1][i] for i in range(maxdice)])
    mult=1
    for m in dpmax:
        mult*=m
    return NumIncreasingSeq/float(mult)

print(probIncreasingSeq([4,6,8]))
print(probIncreasingSeq([4,6,8,10,12,20]))

"""Output:
0.25
0.011792534722222222
"""