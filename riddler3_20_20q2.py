#a: 1 2 3
#b: green purple red
#c: solid striped open
#d: diamond oval squiggle
from itertools import permutations

Set1 = [(1,3,1,3),(2,3,1,3),(3,3,1,3),(1,3,1,1),(2,3,1,1),(3,3,1,1),(1,3,1,2),(2,3,1,2),(3,3,1,2)]
Set2 = [(1,1,1,3),(2,1,1,3),(3,1,1,3),(1,1,1,1),(2,1,1,1),(3,1,1,1),(1,1,1,2),(2,1,1,2),(3,1,1,2)]
Set3 = [(1,2,1,3),(2,2,1,3),(3,2,1,3),(1,2,1,1),(2,2,1,1),(3,2,1,1),(1,2,1,2),(2,2,1,2),(3,2,1,2)]
Set4 = [(1,3,2,3),(2,3,2,3),(3,3,2,3),(1,3,2,1),(2,3,2,1),(3,3,2,1),(1,3,2,2),(2,3,2,2),(3,3,2,2)]
Set5 = [(1,1,2,3),(2,1,2,3),(3,1,2,3),(1,1,2,1),(2,1,2,1),(3,1,2,1),(1,1,2,2),(2,1,2,2),(3,1,2,2)]
Set6 = [(1,2,2,3),(2,2,2,3),(3,2,2,3),(1,2,2,1),(2,2,2,1),(3,2,2,1),(1,2,2,2),(2,2,2,2),(3,2,2,2)]
Set7 = [(1,3,3,3),(2,3,3,3),(3,3,3,3),(1,3,3,1),(2,3,3,1),(3,3,3,1),(1,3,3,2),(2,3,3,2),(3,3,3,2)]
Set8 = [(1,1,3,3),(2,1,3,3),(3,1,3,3),(1,1,3,1),(2,1,3,1),(3,1,3,1),(1,1,3,2),(2,1,3,2),(3,1,3,2)]
Set9 = [(1,2,3,3),(2,2,3,3),(3,2,3,3),(1,2,3,1),(2,2,3,1),(3,2,3,1),(1,2,3,2),(2,2,3,2),(3,2,3,2)]

uniques=[]
Allpatterns = [Set1,Set2,Set3,Set4,Set5,Set6,Set7,Set8,Set9]

def validset(c1,c2,c3):
    validset=True
    for f in range(4):
        if (c1[f]+c2[f]+c3[f])%3 != 0:
            validset=False
            return validset
    return validset
            

def finishset(c1,c2,Allpatterns,SetsUsed):
    for c in range(9):
        for p in Allpatterns[c]:
            if validset(c1,c2,p):
                return (p,c)
            
def maximizesetcount(CardsUsed,Allpatterns):
    maxsets=0
    sset=0
    for c in range(9):
        for p in range(9):
            num=0
            perms=permutations([x for x in range(len(CardsUsed))],2)
            cardnum=c*9+p
            testcard=Allpatterns[c][p]
            if testcard not in CardsUsed:
                for pe in list(perms):
                    card1=CardsUsed[pe[0]]
                    card2=CardsUsed[pe[1]]
                    if validset(card1,card2,testcard):
                        num+=1
                if num>=maxsets:
                    maxsets=num
                    choice=testcard
                    sset=c
    return (choice,sset,maxsets)

print("Question2")
            
def findmaxsets(Allpatterns,Set1,Set2,Set3,Set4,Set5,Set6,Set7,Set8,Set9):
    card1=Set1[0]
    card2=Set2[0]
    SetsUsed=[1,1,0,0,0,0,0,0,0]
    #Cards=[item for sublist in Allpatterns for item in sublist]
    choice=finishset(card1,card2,Allpatterns,SetsUsed)
    card3=choice[0]
    SetsUsed[choice[1]]=1
    CardsUsed=[card1,card2,card3]
    numsets=1
    turn=4
    while turn<13:
        choice_cardnum=maximizesetcount(CardsUsed,Allpatterns)
        numsets+=choice_cardnum[2]/2
        CardsUsed.append(choice_cardnum[0])
        SetsUsed[choice_cardnum[1]]=1
        turn+=1
    print(numsets,CardsUsed)
findmaxsets(Allpatterns,Set1,Set2,Set3,Set4,Set5,Set6,Set7,Set8,Set9)

#Output:
#Question2
#14.0 [(1, 3, 1, 3), (1, 1, 1, 3), (1, 2, 1, 3), (3, 2, 3, 2), (2, 2, 2, 1), (3, 1, 3, 2), (3, 3, 3, 2), (2, 1, 2, 1), (2, 3, 2, 1), (2, 2, 3, 2), (1, 2, 3, 2), (2, 1, 3, 2)]