arrs=zeros(100000,1);
for i=1:100000
times=15.*rand(4,1);
T=times(1);
A=times(2);
B=times(3);
C=times(4);
customer=randi([1 4]);
if min(times)==T %tiffany is first to finish
    wait=T+15;
elseif min(times)~=T && customer==1 %another barber is first, customer is for tiff
    wait=T+15;
elseif min(times)~=T && customer~=1 %another barber finishes first, customer takes
    wait=T;
end
arrs(i)=wait;
end
mean(arrs)
histogram(arrs)
