def leapyear(year):
    leap=False
    d4=year%4
    d100=year%100
    d400=year%400
    if d4==0 and d100!=0:
        leap=True
    if d400==0:
        leap=True
    return leap

def getpalindromicdates(s,n):
    monthdays=[31,28,31,30,31,30,31,31,30,31,30,31]
    for year in range(s,int((s+100)/100)*100):
        mm=(year%10)*10+int((year%100)/10)
        dd=int((year%1000)/100)*10+int(year/1000)
        if leapyear(year):
            monthdays[1]=29
        else:
            monthdays[1]=28
        if mm>0 and mm<13 and dd>0 and dd<=monthdays[mm-1]:
            print("{:02d}".format(mm),'/',"{:02d}".format(dd),'/',year)
            n+=1
    return n

            
def getpalindromicdates2(s,n):
    monthdays=[31,28,31,30,31,30,31,31,30,31,30,31]
    for year in range(s,int((s+100)/100)*100):
        dd=(year%10)*10+int((year%100)/10)
        mm=int((year%1000)/100)*10+int(year/1000)
        if leapyear(year):
            monthdays[1]=29
        else:
            monthdays[1]=28
        if mm>0 and mm<13 and dd>0 and dd<=monthdays[mm-1]:
            print("{:02d}".format(dd),'/',"{:02d}".format(mm),'/',year)
            n+=1
    return n
                        
print("^number of additional palindromic dates MM/DD/YYYY ", getpalindromicdates(2020,0)-1)
print("\n")
print("^number of additional palindromic dates DD/MM/YYYY ", getpalindromicdates2(2020,0)-1)

"""Output:

02 / 02 / 2020
12 / 02 / 2021
03 / 02 / 2030
04 / 02 / 2040
05 / 02 / 2050
06 / 02 / 2060
07 / 02 / 2070
08 / 02 / 2080
09 / 02 / 2090
^number of additional palindromic dates MM/DD/YYYY  8


02 / 02 / 2020
12 / 02 / 2021
22 / 02 / 2022
03 / 02 / 2030
13 / 02 / 2031
23 / 02 / 2032
04 / 02 / 2040
14 / 02 / 2041
24 / 02 / 2042
05 / 02 / 2050
15 / 02 / 2051
25 / 02 / 2052
06 / 02 / 2060
16 / 02 / 2061
26 / 02 / 2062
07 / 02 / 2070
17 / 02 / 2071
27 / 02 / 2072
08 / 02 / 2080
18 / 02 / 2081
28 / 02 / 2082
09 / 02 / 2090
19 / 02 / 2091
29 / 02 / 2092
^number of additional palindromic dates DD/MM/YYYY  23
"""
