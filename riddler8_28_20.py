import math
import operator as op
from functools import reduce

def ncr(n, r):
    #https://stackoverflow.com/questions/4941753/is-there-a-math-ncr-function-in-python
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer // denom  # or / in Python 2

def f(x):
    if x<0:
        print("error")
    return math.factorial(x)/(math.factorial(x/2)*2**(x/2))

def coeff(N,k):
    c=0
    l=int(N/4)
    if k<=l:
        for m in range(0,int(math.floor(k/2))+1):
            c+=ncr(l,m)*ncr(l-m,k-2*m)*3**m*6**(k-2*m)
    else:
        for m in range(min(int(math.floor(k/l)),int(math.floor(k/2))),int(math.floor(k/2))+1):
            c+=ncr(l,m)*ncr(l-m,k-2*m)*3**m*6**(k-2*m)
    return c

def main(N):
    exclude=0
    pairs=int(N/2)
    for k in range(1,pairs+1):
        exclude+=f(N-2*k)*coeff(N,k)*(-1)**(k-1)
    ans=f(N)-exclude
    p=(ans/f(N)*0.5**(N/2))
    print(p)
    print(1/p)

main(4*13)

"""
Output:
3.132436174298871e-09
319240343.41220975
"""