import numpy as np
import matplotlib.pyplot as plt
import math
N=100 #resolution of vertical cross section of pen stroke (along 1 radius)
numstrokes=10

#darkness is proportional to circle cross-section
#gives the vertical cross section of 1 stroke (along 1 diameter), graphs the stroke
def gengradient(N):
    rvals=np.linspace(0,1,N)
    gradient=np.zeros(N)
    gradr=np.flip(gradient)
    for i in range(len(rvals)):
        r=rvals[i]
        gradient[i]=math.sqrt(1.0-r**2)
    stroke=np.concatenate([gradr,gradient])
    plt.plot(stroke)
    plt.show()
    return stroke

#test various stroke separations
#returns the best separation, graphs the best solution
def overlapstrokes(Nstrokes,N):
    strokelen=2*N
    uniformity=100
    beststrokesep=-1
    bestprofile=None
    for strokesep in range(N,2*N+1):
        #overlap=strokelen-srokesep
        ctrlocations=np.ones(1)*N
        for offset in range(Nstrokes-1):
            ctrlocations=np.append(ctrlocations,ctrlocations[-1]+strokesep)
        canvasdist=strokesep*(Nstrokes-1)+2*N
        canvaspixs=np.zeros(canvasdist)
        for i in ctrlocations:
            for j in range(int(i)-N,int(i)+N):
                canvaspixs[j]+=stroke[j-(int(i)-N)]
        canvaspixs=canvaspixs[N:-N]
        stddev=np.std(canvaspixs)
        if stddev<uniformity:
            uniformity=stddev
            beststrokesep=strokesep
            bestprofile=canvaspixs
    plt.plot(bestprofile)
    plt.show()
    return beststrokesep,uniformity

stroke=gengradient(N)
separation,dev=overlapstrokes(numstrokes,N)
print("best overlap",2.0-float(separation)/N,"with stddev",dev)
    