def getopenlanes(lanes,N):
    opens=[]
    for i in range(N):
        if lanes[i]==0:
            if i==0:
                if lanes[i+1]==0:
                    opens.append(i)
            elif i==N-1:
                if lanes[i-1]==0:
                    opens.append(i)
            else:
                if lanes[i+1]==0 and lanes[i-1]==0:
                    opens.append(i)
    return opens

def expectedfilled(lanes,N,filled,prob):
    #print(lanes,N,filled,prob)
    openlanes=getopenlanes(lanes,N)
    numopen=len(openlanes)
    if numopen==0:
        global total
        total+=prob*filled
    else:
        for o in openlanes:
            lanes[o]=1
            newprob=1.0/float(numopen)*prob
            expectedfilled(lanes,N,filled+1,newprob)
            lanes[o]=0

for N in range(2,11):
    total=0
    expectedfilled([0 for x in range(N)],N,0,1)
    print("For ", N, "lanes, expect ",total)

"""
For  2 lanes, expect  1.0
For  3 lanes, expect  1.6666666666666665
For  4 lanes, expect  2.0
For  5 lanes, expect  2.466666666666667
For  6 lanes, expect  2.888888888888889
For  7 lanes, expect  3.3238095238095218
For  8 lanes, expect  3.755555555555553
For  9 lanes, expect  4.1880070546737205
For  10 lanes, expect  4.620317460317447
"""