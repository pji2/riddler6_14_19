import random
import urllib.request
data =  urllib.request.urlopen("https://norvig.com/ngrams/enable1.txt")
dictionary=[]
for line in data:
    text=line[:-1].decode('utf-8')
    if len(text)==5:
        dictionary.append(text)
print(len(dictionary))


def lettermatrix(nwords,alphabet):
    letterspresent=[[0 for x in range(len(nwords))] for x in range(len(alphabet))]
    validwords=[]
    for w in range(len(nwords)):
        word=nwords[w]
        subset=set(list(word))
        if not subset.issubset(alphabet):
            continue
        validwords.append(word)
        for l in range(len(alphabet)):
            letter=alphabet[l]
            if letter in word:
                letterspresent[l][w]=1
    return letterspresent,validwords

            
def pick5uniquewords2(n,letterspresent,wordlist,dic,alphabet):
    if n==4:
        print(wordlist)
        return
    picks=dic[:10]
    for wpind in range(len(picks)):
        wp=picks[wpind]
        alphabet_copy=alphabet[:]
        #for each word picked, remove the letters in the word from the alphabet
        #recaculate letter matrix with the allow letters
        for lwpind in range(len(wp)):
            lwp=wp[lwpind]
            alphabet_copy.remove(lwp)
        newletterspresent,newwords=lettermatrix(nwords,alphabet_copy)
        wordlist.append(wp)
        pick5uniquewords2(n+1,newletterspresent,wordlist,newwords,alphabet_copy)
        wordlist.pop()

#choose a couple candidate proposals such that no word in the proposal shares 2 of the same letters
        
alphabet=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
nwords=[]
for word in dictionary:
    if len(word)==len(set(word)):
        nwords.append(word)
letterspresent,validwords=lettermatrix(nwords,alphabet)
arr=[]
pick5uniquewords2(0,letterspresent,arr,nwords,alphabet)

def lettermatch(proposal,mysteryword):
    matchinds=[]
    comp=""
    for i in range(len(proposal)):
        if proposal[i]==mysteryword[i]:
            matchinds.append(i)
            comp+="!"
        else:
            comp+=proposal[i]
    return matchinds,comp

def hasletter(proposal,mystery,matchinds,comp):
    hasinds=[]
    for i in range(len(proposal)):
        if i not in matchinds and mystery[i] in comp:
            compind=comp.index(mystery[i])
            hasinds.append(compind)
            comp=comp[:compind]+"?"+comp[compind+1:]
    return sorted(hasinds)

def includedletters(proposal,matchinds,hasinds,goal):
    includedl=[goal[0]]
    for i in range(5):
        if i in matchinds or i in hasinds:
            includedl.append(proposal[i])
    return includedl

def letterfreqdict(word):
    wfreq=dict()
    for w in word:
        if w not in wfreq:
            wfreq[w]=1
        else:
            wfreq[w]+=1
    return wfreq

#excluded letters are letters in proposal that are not in included
#also exclude multiples of letters if multiple of a letter in proposal>multiple of letter in mystery
def excludeguide(proposal,mystery,included):
    excludedict=dict()
    pfreq=letterfreqdict(proposal)
    ifreq=letterfreqdict(included)
    for p in pfreq:
        if p not in included or pfreq[p]>ifreq[p]:
            excludedict[p]=pfreq[p]
    return excludedict


def possiblewords(proposal,goal,dic):
    matches,comp=lettermatch(proposal,goal)
    present=hasletter(proposal,goal,matches,comp)
    ils=includedletters(proposal,matches,present,goal)
    edict=excludeguide(proposal,goal,ils)
    possible=[]
    for d in dic:
        m,c=lettermatch(proposal,d)
        h=hasletter(proposal,d,m,c)
        dfreq=letterfreqdict(d)
        exclude=0
        for test in dfreq:
               if test in edict and dfreq[test]>=edict[test]:
                    exclude=1
                    break
        if matches==m and present==h and d[0]==goal[0] and not exclude:
            possible.append(d)
    return possible

def finalwords(proposals,goal,dictionary):
    for prop in proposals:
        res=possiblewords(prop,goal,dictionary)
        if len(res)<=1:
            break
        dictionary=res
    return res

def getwinprob(proposals,dictionary):
    assert(len(set(dictionary))==8636)
    dic=dictionary[:]
    dic=[]
    accuracysample=1000
    for i in range(accuracysample):
        rand=random.randint(0,len(dictionary)-1)
        dic.append(dictionary[rand])
        #del dictionary[rand]
    
    denom=len(dic)
    correct=0
    for goal in dic:
        res=finalwords(proposals,goal,dic)
        correct+=1.0/len(res)
    return correct/accuracysample

#get win probability of the proposals. Note that this
#limits to test 1000 random words in the dictionary to save time
def getwinprob(proposals,dictionary):
    assert(len(set(dictionary))==8636)
    dic=dictionary[:]
    dic=[]
    accuracysample=1000
    for i in range(accuracysample):
        rand=random.randint(0,len(dictionary)-1)
        dic.append(dictionary[rand])
        #del dictionary[rand]
    
    denom=len(dic)
    correct=0
    for goal in dic:
        res=finalwords(proposals,goal,dic)
        correct+=1.0/len(res)
    return correct/accuracysample

uniquewords=[['abets', 'child', 'frown', 'jumpy'],
['abets', 'child', 'frump', 'wonky'],
['abets', 'child', 'grown', 'jumpy'],
['abets', 'child', 'grump', 'wonky'],
['abets', 'chimp', 'dorky', 'flung'],
['abets', 'chimp', 'dowry', 'flung'],
['abets', 'chimp', 'dowry', 'flunk'],
['abets', 'chimp', 'drown', 'fluky'],
['abets', 'chimp', 'drown', 'gulfy'],
['abets', 'chimp', 'drunk', 'jowly'],
['abets', 'chimp', 'fjord', 'gunky'],
['abets', 'chink', 'dowry', 'flump'],
['abets', 'chink', 'dumpy', 'growl'],
['abets', 'chink', 'fjord', 'gulpy'],
['abets', 'chink', 'fjord', 'lumpy'],
['abets', 'chink', 'fjord', 'plumy'],
['abets', 'chink', 'flump', 'dowry'],
['abets', 'chink', 'flump', 'rowdy'],
['abets', 'chink', 'flump', 'wordy'],
['abets', 'chink', 'frump', 'godly'],
['abets', 'chink', 'frump', 'jowly'],
['abets', 'chirk', 'downy', 'flump'],
['abets', 'chirk', 'dumpy', 'flong'],
['abets', 'chirk', 'dumpy', 'flown'],
['abets', 'chirk', 'flong', 'dumpy'],
['abets', 'chirk', 'flong', 'jumpy'],
['abets', 'chirk', 'flown', 'dumpy'],
['abets', 'chirk', 'flown', 'jumpy'],
['abets', 'chirk', 'flown', 'pudgy'],
['abets', 'chirk', 'flump', 'downy'],
['abets', 'chirm', 'flown', 'pudgy'],
['abets', 'chirm', 'flunk', 'podgy'],
['abets', 'chirp', 'dumky', 'flong'],
['abets', 'chirp', 'dumky', 'flown'],
['abets', 'chirp', 'flong', 'dumky'],
['abets', 'chirp', 'flown', 'dumky'],
['abets', 'chivy', 'drown', 'flump'],
['abets', 'chivy', 'fjord', 'plunk'],
['abets', 'chivy', 'flown', 'grump'],
['abets', 'chivy', 'flump', 'drown'],
['abets', 'chivy', 'flump', 'grown'],
['abets', 'chivy', 'flump', 'wrong'],
['abhor', 'civet', 'flump', 'wynds'],
['abler', 'chink', 'gowds', 'jumpy'],
['abler', 'chomp', 'ditzy', 'funks'],
['abler', 'chomp', 'ditzy', 'gunks'],
['abler', 'chomp', 'ditzy', 'junks'],
['abler', 'chomp', 'ditzy', 'swung'],
['abler', 'chomp', 'dungs', 'wifty'],
['ables', 'chimp', 'fjord', 'gunky'],
['abort', 'chevy', 'dings', 'flump'],
['abort', 'chevy', 'dinks', 'flump'],
['abort', 'chevy', 'djins', 'flump'],
['abort', 'chevy', 'dumps', 'fling'],
['abort', 'chews', 'dingy', 'flump'],
['abort', 'chews', 'dinky', 'flump'],
['abort', 'chews', 'dumky', 'fling'],
['abort', 'chews', 'dumpy', 'fling'],
['abort', 'chews', 'dying', 'flump'],
['abort', 'chews', 'fling', 'dumky'],
['abort', 'chews', 'fling', 'dumpy'],
['abort', 'chews', 'fling', 'jumpy'],
['abort', 'chewy', 'dings', 'flump'],
['abort', 'chewy', 'dinks', 'flump'],
['abort', 'chewy', 'djins', 'flump'],
['abort', 'chewy', 'dumps', 'fling']]
maxprob=0
winner=[]
for u in uniquewords:
    prob=getwinprob(u,dictionary)
    print(u,prob)
    if prob>maxprob:
        maxprob=prob
        winner=u
print(winner,maxprob)

#win probability of the best performing proposal above
#Tests against all words in the dictionary
def getwinprob2(proposals,dictionary):
    assert(len(set(dictionary))==8636)
    dic=dictionary[:]
    denom=len(dic)
    correct=0
    for goal in dic:
        res=finalwords(proposals,goal,dic)
        correct+=1.0/len(res)
    return correct/len(dictionary)
#['abets', 'chimp', 'drunk', 'jowly'] 0.9162806855025439
#['abets', 'chirk', 'flown', 'pudgy'] 0.9217230199166245
print(getwinprob2(['abort', 'chews', 'dinky', 'flump'],dictionary))

#['abort', 'chews', 'dinky', 'flump'] 0.9234599351551614