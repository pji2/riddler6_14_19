from ortools.sat.python import cp_model
def CrossProductConstraintProgram():
    model = cp_model.CpModel()
    
    #Set variables
    twofactors=[[0 for x in range(3)] for y in range(7)]
    for i in range(7):
        for j in range(3):
            twofactors[i][j]=model.NewIntVar(0,3,"2_at_"+str(i)+str(j))
    threefactors=[[0 for x in range(3)] for y in range(7)]
    for i in range(7):
        for j in range(3):
            threefactors[i][j]=model.NewIntVar(0,2,"3_at_"+str(i)+str(j))
    fivefactors=[[0 for x in range(3)] for y in range(7)]
    for i in range(7):
        for j in range(3):
            fivefactors[i][j]=model.NewIntVar(0,1,"5_at_"+str(i)+str(j))
    sevenfactors=[[0 for x in range(3)] for y in range(7)]
    for i in range(7):
        for j in range(3):
            sevenfactors[i][j]=model.NewIntVar(0,1,"7_at_"+str(i)+str(j))
    
    #set constraints for rows
    tworows=[3,3,1,3,2,8,1]
    threerows=[0,1,4,2,1,0,2]
    fiverows=[1,0,0,1,1,0,0]
    sevenrows=[1,1,0,0,0,0,1]
    for i in range(7):
        model.Add(sum(twofactors[i][:])==tworows[i])
    for i in range(7):
        model.Add(sum(threefactors[i][:])==threerows[i])
    for i in range(7):
        model.Add(sum(fivefactors[i][:])==fiverows[i])
    for i in range(7):
        model.Add(sum(sevenfactors[i][:])==sevenrows[i])
    
    #set constraints for columns
    twocolssums=[2,14,5]
    threecolssums=[8,1,1]
    fivecolssums=[0,1,2]
    sevencolssums=[1,0,2]
    for k in range(3):
        model.Add(sum([twofactors[i][k] for i in range(7)])==twocolssums[k])
    for k in range(3):
        model.Add(sum([threefactors[i][k] for i in range(7)])==threecolssums[k])
    for k in range(3):
        model.Add(sum([fivefactors[i][k] for i in range(7)])==fivecolssums[k])
    for k in range(3):
        model.Add(sum([sevenfactors[i][k] for i in range(7)])==sevencolssums[k])
        
    #each entry must be 0-9
    for i in range(7):
        for j in range(3):
            model.Add(3*twofactors[i][j]+5*threefactors[i][j]+10*fivefactors[i][j]+10*sevenfactors[i][j]<11)

    solver = cp_model.CpSolver()
    status = solver.Solve(model)
    
    if status == cp_model.FEASIBLE:
        print("foundsolution")
        for i in range(7):
            rowintegers=""
            for j in range(3):
                integer=2**solver.Value(twofactors[i][j])*3**solver.Value(threefactors[i][j])\
                *5**solver.Value(fivefactors[i][j])*7**solver.Value(sevenfactors[i][j])
                rowintegers+=str(integer)
            print(rowintegers)

CrossProductConstraintProgram()

#Output
"""
foundsolution
785
387
963
985
354
488
927
"""