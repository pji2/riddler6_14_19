from __future__ import print_function
from ortools.linear_solver import pywraplp
#38 electoral votes to win
winthresholds=[6,11,16,21,26,31,36,41,46,51]
ecs=[3,4,5,6,7,8,9,10,11,12]
solver = pywraplp.Solver('simple_mip_program',pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)
vars_=[]
for i in range(10):
    vars_.append(solver.IntVar(0,1,"x"+str(i)))
solver.Add(sum([ecs[x]*vars_[x] for x in range(10)])>=38)
solver.Minimize(sum([winthresholds[x]*vars_[x] for x in range(10)]))
status=solver.Solve()
if status == pywraplp.Solver.OPTIMAL:
    print('Solution:')
    print('Objective value =', solver.Objective().Value())
    for i in range(10):
        if vars_[i].solution_value():
            print("win state ",i+1)
print(sum([winthresholds[i]*vars_[i].solution_value() for i in range(10)])/sum([11,21,31,41,51,61,71,81,91,101]), "popular vote")
print(sum([ecs[i]*vars_[i].solution_value() for i in range(10)]),"electoral votes")

"""
Output:
Solution:
Objective value = 136.0
win state  1
win state  2
win state  3
win state  5
win state  7
win state  8
0.24285714285714285 popular vote
38.0 electoral votes
"""