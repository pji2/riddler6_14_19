def E(S,T): #S unknown squares, T players remaining
    if T==0:
        return 0
    if S==0:
        return T
    e=0.5**S*T
    for i in range(1,S+1):
        e+=0.5**i*E(S-i,T-1)
    return e

print(E(18,16))

"""Output:
7.0000762939453125
"""