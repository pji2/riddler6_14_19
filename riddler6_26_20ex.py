#riddler express
import math
prob=0
for left in range(0,11):
    right=10-left
    if (left-right)%4==0:
        p=math.factorial(10)/(math.factorial(left)*math.factorial(10-left))/2**10
        prob+=p
        print(left,right,prob)
print(prob)

#riddler express EC
prob=0
for straight in range(0,11):
    turns=10-straight
    for left in range(0,turns+1):
        right=turns-left
        if (left-right)%4==0:
            assert(straight+left+right==10)
            p=math.factorial(10)/(math.factorial(straight)*math.factorial(left)*math.factorial(right))/3**10
            prob+=p
            print(straight,left,right,p)
print(prob)
