def getlastarrangements(placement,possletters):
    lastinds=dict()
    for l in possletters:
        ind=placement.rfind(l)
        lastinds[l]=ind
    previousarrangements=[]
    for letterkey in lastinds:
        copy=placement[:]
        i=possletters.index(letterkey)
        new=""
        if i<len(possletters)-1 and i>0: #a middle level
            letterbelow=copy.count(possletters[i-1])
            lettercount=copy.count(possletters[i])
            letterabove=copy.count(possletters[i+1])
            if lettercount<=letterbelow and lettercount>letterabove:
                new=copy[:lastinds[letterkey]]+copy[lastinds[letterkey]+1:]
        elif i==0: #the base level
            lettercount=copy.count(possletters[i])
            letterabove=copy.count(possletters[i+1])
            if lettercount>letterabove:
                new=copy[:lastinds[letterkey]]+copy[lastinds[letterkey]+1:]
        elif i==len(possletters)-1: #top level, we can always remove 1
            new=copy[:lastinds[letterkey]]+copy[lastinds[letterkey]+1:]
        previousarrangements.append(new)
    return previousarrangements

def countways(arrangement):
    lettercounts=[]
    for letter in sorted(list(set(arrangement))):
        lettercounts.append(arrangement.count(letter))
    if lettercounts[0]>0 and all(x==0 for x in lettercounts[1:]):
        return 1
    if all(x==1 for x in lettercounts):
        return 1
    previousarrs=getlastarrangements(arrangement,sorted(list(set(arrangement))))
    ways=0
    for arr in previousarrs:
        if arr!="":
            ways+=countways(arr)
    return ways

print(countways("aaaabbbccd"))

#Output:
#768