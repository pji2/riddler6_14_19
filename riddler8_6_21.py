import math
class state:
    def __init__(self,x,y,Dx,Dy):
        self.x=x
        self.y=y
        self.Dx=Dx
        self.Dy=Dy
        self.steps=0
        
    def setsteps(self,steps):
        self.steps=steps
    
statemap=dict()

        
def validMove(fromstate,tostate):
    if tostate.x<=0 and tostate.y<0 and fromstate.x>=0 and fromstate.y<0: #force counter-clockwise
        return False
    if tostate.x<-7 or tostate.x>7 or tostate.y<-7 or tostate.y>7:
        return False
    if math.sqrt(tostate.x**2+tostate.y**2)<3:
        return False
    #check circle intersection
    if tostate.x==fromstate.x:
        dist=abs(tostate.x)
    else:
        m=(tostate.y-fromstate.y)/(tostate.x-fromstate.x)
        b=tostate.y-m*tostate.x
        a=-m
        c=-b
        dist=abs(c)/math.sqrt(a**2+1)
    if dist<3:
        return False
    return True

def destination(fromstate,tostate,destzone):
    #if tostate.x==dest[0] and tostate.y==dest[1]:
     #   return True
    if fromstate.x<0 and tostate.x>=destzone[0] and tostate.y<=destzone[1]:
        return True
    return False



start=state(0,-5,0,0)
def solve(destzone):
    statequeue=list()
    statequeue.append(start)
    while len(statequeue)>0:
        current=statequeue[0]
        statequeue.pop(0)
        for dx in range(-1,2):
            for dy in range(-1,2):
                newx=current.x+current.Dx+dx
                newy=current.y+current.Dy+dy
                newdx=newx-current.x
                newdy=newy-current.y
                steps=current.steps+1
                newstate=state(newx,newy,newdx,newdy)
                newstate.setsteps(steps)
                if validMove(current,newstate):
                    if newstate not in statemap:
                        statemap[newstate]=current
                        if destination(current,newstate,destzone):
                            return newstate
                        statequeue.append(newstate)
    return state(0,0,0,0)
end=solve((-3,-2))

def solutionprinter(end):
    numsteps=end.steps
    print(end.x,end.y)
    n=statemap[end]
    while n!=start:
        print(n.x,n.y)
        n=statemap[n]
    print(start.x,start.y)
    return numsteps
solutionprinter(end)

"""Output:
-3 -2
-3 1
-3 3
-2 4
0 4
2 3
3 1
3 -2
2 -4
1 -5
0 -5
"""