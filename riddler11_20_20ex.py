#Friday the 13th if 1st of month is on a Sunday
regularyear=[31,28,31,30,31,30,31,31,30,31,30,31]
leapyear=[31,29,31,30,31,30,31,31,30,31,30,31]
fouryears1=[regularyear,regularyear,regularyear,regularyear] #max8 for Mon
fouryears2=[leapyear,regularyear,regularyear,regularyear] #max 9 for Sun
fouryears3=[regularyear,leapyear,regularyear,regularyear] #max 8 for Sun,Thurs
fouryears4=[regularyear,regularyear,leapyear,regularyear] #max 8 for Sun
fouryears5=[regularyear,regularyear,regularyear,leapyear] #max 8 for Sun,Thurs
period=[item for s in fouryears2 for item in s] #test fouryears1, ... fouryears5
for initday in range(7):
    #print("initday",initday)
    numFriThirteenth=0
    firstdayofmonth=initday
    if firstdayofmonth==0:
            numFriThirteenth+=1
    for i in range(len(period)-1):
        firstdayofmonth=(firstdayofmonth+period[i])%7
        if firstdayofmonth==0:
            numFriThirteenth+=1
        #print(firstdayofmonth)
    print(initday,"number of friday the thirteenths:", numFriThirteenth)

#Output
"""
0 number of friday the thirteenths: 9
1 number of friday the thirteenths: 7
2 number of friday the thirteenths: 6
3 number of friday the thirteenths: 6
4 number of friday the thirteenths: 7
5 number of friday the thirteenths: 7
6 number of friday the thirteenths: 6
"""

#EC
regularyear=[31,28,31,30,31,30,31,31,30,31,30,31]
leapyear=[31,29,31,30,31,30,31,31,30,31,30,31]
fouryears1=[regularyear,regularyear,regularyear,regularyear] 
fouryears2=[leapyear,regularyear,regularyear,regularyear] #max 9 for Jan
fouryears3=[regularyear,leapyear,regularyear,regularyear] #max 9 for May 
fouryears4=[regularyear,regularyear,leapyear,regularyear] 
fouryears5=[regularyear,regularyear,regularyear,leapyear]
period=[item for s in fouryears3 for item in s] #test fouryears1, ... fouryears5
for initmonth in range(12):
    numFriThirteenth=1
    ThirteenthDay=5
    #print("initmonth",initmonth)
    for i in range(initmonth,initmonth+len(period)-1):
        ThirteenthDay=(ThirteenthDay+period[i%48])%7
        if ThirteenthDay==5:
            numFriThirteenth+=1
        #print(ThirteenthDay)
    print(initmonth,"number of friday the thirteenths:",numFriThirteenth)

#Output:
"""
0 number of friday the thirteenths: 8
1 number of friday the thirteenths: 8
2 number of friday the thirteenths: 7
3 number of friday the thirteenths: 7
4 number of friday the thirteenths: 9
5 number of friday the thirteenths: 7
6 number of friday the thirteenths: 7
7 number of friday the thirteenths: 7
8 number of friday the thirteenths: 7
9 number of friday the thirteenths: 8
10 number of friday the thirteenths: 7
11 number of friday the thirteenths: 7
"""