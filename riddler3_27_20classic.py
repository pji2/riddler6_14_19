import itertools as it
import numpy as np

def partition(number):
    #https://stackoverflow.com/questions/10035752/elegant-python-code-for-integer-partitioning
    answer = set()
    answer.add((number, ))
    for x in range(1, number):
        for y in partition(number - x):
            answer.add(tuple(sorted((x, ) + y)))
    return answer

def multinomial(lst):
    #https://stackoverflow.com/questions/46374185/does-python-have-a-function-which-computes-multinomial-coefficients
    res, i = 1, 1
    for a in lst:
        for j in range(1,a+1):
            res *= i
            res //= j
            i += 1
    return res

#Calculates transition probabilities
def transitionprob(old_arrangement,new_arrangement,N):
    freq_dict={}
    for i in range(len(old_arrangement)):
        freq_dict[i+1]=old_arrangement[i]

    numberchoices=list(it.combinations([x for x in range(1,len(old_arrangement)+1)],len(new_arrangement)))
    arrangement_distri=list(it.permutations([x for x in range(len(new_arrangement))]))
    unique_arrangements=[]
    for n in numberchoices:
        for a in arrangement_distri:
            newentry=[(n[i],new_arrangement[a[i]]) for i in range(len(new_arrangement))]
            if newentry not in unique_arrangements:
                unique_arrangements.append(newentry)

    prob=0
    for arr in unique_arrangements:
        multiplier=1.0
        for x,y in arr:
            multiplier*=(freq_dict[x]/float(N))**y
        prob+=multinomial(new_arrangement)*multiplier
    return prob

#Constructs transition matrix
def main(N):
    Partitions=sorted([list(l) for l in list(partition(N))])
    states=len(Partitions)
    Tmat=np.zeros((states,states))
    for i in range(states):
        for j in range(states):
            partitionfrom=Partitions[i]
            partitionto=Partitions[j]
            if len(partitionto)<=len(partitionfrom):
                prob=transitionprob(partitionfrom,partitionto,N)
                Tmat[i,j]=prob
    #print(Tmat.sum(axis=1))
    mat=states-1
    Q=Tmat[0:mat,0:mat]
    N=np.linalg.inv(np.eye(mat)-Q)
    t=np.matmul(N,np.ones((mat,1)))
    print("number of rolls until all numbers are the same: ",t.item(0))

main(6)

"""
Output:

('number of rolls until all numbers are the same: ', 9.655991483885568)
"""