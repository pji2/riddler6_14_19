def solvediophantine1var(numer, denom): #solve numer/denom = 2/c
    c = 1
    solution = []
    #Do we have to increase c? (if numer/denom<2/c, we must increase c)
    while 2*denom > c*numer:
        c += 1
    #we found the solution for the 1-term case
    if 2*denom == c*numer:
            solution.append(c)
    return solution

def solvediophantine2var(numer, denom, A): #solve numer/denom = 2/b + 2/c
    #we choose a candidate for b and then use solvediophantine1var() to find c
    solution = []
    b = 1
    #Test various values for b. Once 2*2/b is less than numer/denom, we 
    #know we can't find any more solutions by increasing b because we want c>=b
    while 2*2.0/b > float(numer)/float(denom):
        b += 1
        #is there room for a second term after subtracting one term?
        if float(numer)/float(denom)-2.0/b > 0:
            #the numer and denom to set equal to 2/c, equivalent to "numer/denom-2/b = 2/c"
            new_numer = float(numer)*b - 2*float(denom)
            new_denom = float(denom)*b
            #find candidates for c
            sol = solvediophantine1var(new_numer, new_denom)
            for c in sol:
                if c >= b and b >= A: #count distinct solutions where a<=b<=c
                    solution.append([b, c])
    return solution

def verifysolution(a,b,c):
    return a*b*c == 2*a*b + 2*b*c + 2*a*c

solutions = [solvediophantine2var(1,3,3),solvediophantine2var(1,2,4),solvediophantine2var(3,5,5),solvediophantine2var(2,3,6)]
for a in range(3,3+len(solutions)):
    for ss in solutions[a-3]:
        assert(verifysolution(a,ss[0],ss[1]))

print("with A=3, b, c can equal: ", solutions[0])
print("with A=4, b, c can equal: ", solutions[1])
print("with A=5, b, c can equal: ", solutions[2])
print("with A=6, b, c can equal: ", solutions[3])

#number of distinct triplets: 10
#(3,7,42) (3,8,24) (3,9,18) (3,10,15) (3,12,12) (4,5,20) (4,6,12) (4,8,8) (5,5,10) (6,6,6)

#output:
#with A=3, b, c can equal:  [[7, 42], [8, 24], [9, 18], [10, 15], [12, 12]]
#with A=4, b, c can equal:  [[5, 20], [6, 12], [8, 8]]
#with A=5, b, c can equal:  [[5, 10]]
#with A=6, b, c can equal:  [[6, 6]]