ave=1/10*0+9/100*4.5;
for numdigits=2:4
    prob=(9/10)^numdigits*(1/10);
    for n=10^(numdigits-1)+1:10^(numdigits)-1
       result=0;
       num=num2str(n);
       if sum(num=='0')>0
           continue
       end
       min=10;
       added=10;
       for i=1:numdigits
           d=str2double(num(i));
           if d<=min
               result=result+d/added;
               min=d;
               added=added*10;
           end
       end
       result;
       samplespace=9^numdigits;
       ave=ave+result*prob*1/samplespace;
    end
end
ave
%.5513 for 5 agrees with riddler11_15_19_2!
%.5201 for 4