import csv
import numpy
markovmatrix=[]
#https://bitbucket.org/pji2/riddler6_14_19/src/master/riddler2_5_21matrix.csv
with open('riddler2_5_21matrix.csv', 'r') as file:
    reader = csv.reader(file)
    for row in reader:
        newrow=[]
        for i in range(len(row)):
            if row[i]=='0.5':
                newrow.append(0.5)
            elif row[i]=='0.33':
                newrow.append(1.0/3)
            elif row[i]=='1':
                newrow.append(1)
            else:
                newrow.append(0)
        markovmatrix.append(newrow)

terminalstates=[13,26]
markovmatrix_=numpy.zeros((25,25))
rownum=-1
for i in range(27):
    if i in terminalstates:
        continue
    rownum+=1
    colnum=-1
    for j in range(27):
        if j in terminalstates:
            continue
        colnum+=1
        markovmatrix_[rownum,colnum]=markovmatrix[i][j]
        

I=numpy.eye(25)
N=numpy.linalg.inv(I-markovmatrix_)
print(numpy.sum(N[0]))

#Output
#70.77777777777759