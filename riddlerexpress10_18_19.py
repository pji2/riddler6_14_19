grid=[[6,2,1,3,6,1,7,7,4,3],[2,3,4,5,7,8,1,5,2,3],[1,6,1,2,5,1,6,3,6,2],[5,3,5,5,1,6,7,3,7,3],\
     [1,2,6,4,1,3,3,5,5,5],[2,4,6,6,6,2,1,3,8,8],[2,4,100,2,3,6,5,2,4,6],[3,1,7,6,2,3,1,5,7,7],\
      [6,1,3,6,4,5,4,2,2,7],[6,7,5,7,6,2,4,1,9,1]]
visited=[[0 for x in range(10)] for y in range(10)]
visited[9][0]=1
def maze(grid, visited, curr, path,iters):
    x=curr[0];y=curr[1];
    visited[x][y]=1
    if iters>10:
        return False
    val=grid[x][y]
    if val==100:
        visited[curr[0]][curr[1]]=1
        print(path)
        return True
    for diffx in [-1,1]:
        new = [x+diffx*val,y]
        if new[0]>-1 and new[0]<10:
            if maze(grid, visited, new, path+str(val),iters+1):
                visited[new[0]][new[1]]=1
                return True
            visited[new[0]][new[1]]=0
    for diffy in [-1,1]:
        new = [x,y+diffy*val]
        if new[1]>-1 and new[1]<10:
            if maze(grid, visited, new, path+str(val),iters+1):
                visited[new[0]][new[1]]=1
                return True
            visited[new[0]][new[1]]=0
    return False


print(maze(grid,visited,[9,0],"",0))

#Output
"""

656434362
True

"""