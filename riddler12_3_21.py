#equations from https://math.stackexchange.com/questions/885546/area-of-the-polygon-formed-by-cutting-a-cube-with-a-plane
import math

vertexdict={(1,0,0):1,(0,1,0):1,(0,0,1):1,(1,1,1):1,(1,1,0):0,(0,1,1):0,(1,0,1):0,(0,0,0):0}
vi=[(1,0,0),(0,1,0),(0,0,1),(1,1,1),(1,1,0),(0,1,1),(1,0,1),(0,0,0)]
def theta(t):
    if t<0:
        return 0
    return t

def phi(t,m1,m2,m3):
    return 1.0/(6*m1*m2*m3)*theta(t)**3
    
def dot(a,b):
    d=0
    for i in range(3):
        d+=a[i]*b[i]
    return d
    
def volume(b,m1,m2,m3):
    V=0
    for i in range(8):
        vertex=vi[i]
        v=(-1)**vertexdict[vertex]*phi(b-dot((m1,m2,m3),vertex),m1,m2,m3)
        V+=v
    return V
    
def SAsliceface(b,m1,m2,m3):
    SA=0
    for i in range(8):
        vertex=vi[i]
        s=(-1)**vertexdict[vertex]*theta(b-dot((m1,m2,m3),vertex))**2
        SA+=s
    return 1.0/(2*m1*m2*m3)*SA

def tetrahedronslice(A,B,C):
    #A>1,B>1,C>1
    norm=math.sqrt(A**2+B**2+C**2)
    m1=A/norm
    m2=B/norm
    m3=C/norm
    b=1/norm
    V=volume(b,m1,m2,m3)
    SAslice=SAsliceface(b,m1,m2,m3)
    SA=6-0.5*(1.0/A*1.0/B+1.0/A*1.0/C+1.0/B*1.0/C)+SAslice
    Vcomp=1-V
    return SA/Vcomp

minval=6
mins=(0,0,0)
for s in range(100,500):
    s_=s/100.0
    val=tetrahedronslice(s_,s_,s_)
    if val<minval:
        minval=val
        mins=(1.0/s_,1.0/s_,1.0/s_)
print("tetrahedron slice", minval,mins)

def trapezoidslice(A,B,C):
    #A>1, B>1, C<1
    norm=math.sqrt(A**2+B**2+C**2)
    m1=A/norm
    m2=B/norm
    m3=C/norm
    b=1/norm
    V=volume(b,m1,m2,m3)
    SAslice=SAsliceface(b,m1,m2,m3)
    r1=(b-m3)/m2
    r2=(b-m3)/m1
    SA=6-0.5*r1*r2-0.5*1/A*1/B-0.5*(1/B+r1)-0.5*(1/A+r2)+SAslice
    Vcomp=1-V
    return SA/Vcomp

minval=6
mins=(0,0,0)
for s in range(100,1000,3):
    s_=s/100.0
    for c in range(1,100,3):
        c_=c/100.0
        val=trapezoidslice(s_,s_,c_)
        #print(s_,c_,val)
        if val<minval:
            minval=val
            mins=(1.0/s_,1.0/s_,1.0/c_)
print("trapezoid slice",minval,mins)  

def hexagonslice(A,B,C):
    #A<1, B<1, C<1
    norm=math.sqrt(A**2+B**2+C**2)
    m1=A/norm
    m2=B/norm
    m3=C/norm
    b=1/norm
    Vbelow=volume(b,m1,m2,m3)
    SAslice=SAsliceface(b,m1,m2,m3)
    r1=(b-m3)/m2
    r2=(b-m3)/m1
    r3=(b-m1)/m3
    r4=(b-m1)/m2
    r5=(b-m2)/m1
    r6=(b-m2)/m3
    h12=1/C-1
    h34=1/A-1
    h56=1/B-1
    SAs=(0.5*1/A*1/B-(0.5*h34*r4+0.5*h56*r5))+(0.5*1/B*1/C-(0.5*h56*r6+0.5*h12*r1))+(0.5*1/C*1/A-(0.5*h12*r2+0.5*h34*r3))+0.5*r1*r2+0.5*r3*r4+0.5*r5*r6
    SAbelow=SAs+SAslice
    SAabove=6-SAs+SAslice
    Vabove=1-Vbelow
    if Vabove>Vbelow:
        #print("above")
        return SAabove/Vabove
    else:
        return SAbelow/Vbelow
minval=6
mins=(0,0,0)
for s in range(500,1000):
    s_=s/1000.0
    val=hexagonslice(s_,s_,s_)
    #print(s_,val)
    if val<minval:
        minval=val
        mins=(1.0/s_,1.0/s_,1.0/s_)
print("hexagon slice",minval,mins)  

def pentagonslice(A,B,C):
    #A<1, B<1, C>1
    norm=math.sqrt(A**2+B**2+C**2)
    m1=A/norm
    m2=B/norm
    m3=C/norm
    b=1/norm
    Vbelow=volume(b,m1,m2,m3)
    SAslice=SAsliceface(b,m1,m2,m3)
    r3=(b-m1)/m3
    r4=(b-m1)/m2
    r5=(b-m2)/m1
    r6=(b-m2)/m3
    SAs=0.5*r3*r4+0.5*r5*r6+(1-0.5*(1-r4)*(1-r5))+0.5*(1/C+r3)+0.5*(1/C+r6)
    SAbelow=SAs+SAslice
    SAabove=6-SAs+SAslice
    Vabove=1-Vbelow
    if r3>1 or r4>1 or r5>1 or r6>1:
        return 1000
    if Vabove>Vbelow:
        return SAabove/Vabove
    else:
        return SAbelow/Vbelow
    
minval=6
mins=(0,0,0)
for s in range(1,100):
    s_=s/100.0
    for c in range(100,1000):
        c_=c/100.0
        val=pentagonslice(s_,s_,c_)
        #print(1.0/s_,1.0/c_,val)
        if val<minval:
            minval=val
            mins=(1.0/s_,1.0/s_,1.0/c_)
print("pentagon slice",minval,mins) 

"""Output:
tetrahedron slice 5.961764707249975 (0.425531914893617, 0.425531914893617, 0.425531914893617)
trapezoid slice 5.956636884604047 (0.1485884101040119, 0.1485884101040119, 100.0)
hexagon slice 6 (0, 0, 0)
pentagon slice 6 (0, 0, 0)
"""
