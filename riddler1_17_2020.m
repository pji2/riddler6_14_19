%Riddler Classic 1-17-20
%see https://www.dartmouth.edu/~chance/teaching_aids/books_articles/probability_book/Chapter11.pd, pg 417
format long
mat=[0 0 .25 0 .5 0 .25;
0 0 .25 0 .5 0 .25;
0 4/9 0 2/9 0 2/9 1/9;
0 0 .5 0 .5 0 0;
0 4/9 0 1/9 0 2/9 2/9;
0 0 .25 0 .5 0 .25;
0 0 0 0 0 0 1];
Q=mat(1:6,1:6);
N=eye(6,6)-Q;
s=[1 0 0 0 0 0]/N;
sum(s)
%4.9054054
%363/74

%Riddler Express
clear
maxscore=0;
maxpo1=0;
for po1=0:100
    minscore=100;
    for pd1=0:100
        val=.4*pd1/100+.2*po1/100-.5*pd1*po1/100^2+4/5;
        if val<minscore
            minscore=val;
            minpd1=pd1/100;
        end
    end
    if minscore>maxscore
        maxscore=minscore;
        maxpo1=po1/100;
    end
end
maxscore
maxpo1
%maxscore=.96
%maxpo1=.80