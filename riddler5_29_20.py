import math
N=10
assert(N%2==0)
pairingslist=[]
ind=0
#lists all possible pairings of 2*n elements
print("The number of ways to pair ", N, " items is ", math.factorial(N)/((2**(N/2))*math.factorial(N/2)))
def createPairings(nums, length, pos):
    if pos==length:
        pairingslist.append([])
        for i in range(0,length,2):
            global ind
            pairingslist[ind].append([min(nums[i],nums[i+1]),max(nums[i],nums[i+1])])
        ind+=1
        return
    for i in range(pos+1,length):
        temp=nums[pos+1]
        nums[pos+1]=nums[i]
        nums[i]=temp
        createPairings(nums,length,pos+2)
        temp=nums[i]
        nums[i]=nums[pos+1]
        nums[pos+1]=temp

createPairings([x+1 for x in range(N)],N,0)
#print(pairingslist)
            
#find how many of these pairings have at least 1 interval that overlaps all others
valid=0
for arrangement in pairingslist:
    #evaluate each interval to see if it overlaps all others
    for i in range(len(arrangement)):
        int1=arrangement[i]
        overlap=True
        for j in range(len(arrangement)):
            int2=arrangement[j]
            if not (int2[1]>=int1[0] and int2[0]<=int1[1]):
                overlap=False
        if overlap:
            break
    if overlap:
        #print(arrangement)
        valid+=1
print(valid,len(pairingslist))
print(float(valid)/len(pairingslist))
#answer is 2/3rds regardless of n

#find how many of these pairings have at least 2 intervals that overlaps all others
valid=0
for arrangement in pairingslist:
    #evaluate each interval to see if it overlaps all others
    overlappingarrs=0
    for i in range(len(arrangement)):
        int1=arrangement[i]
        overlap=True
        for j in range(len(arrangement)):
            int2=arrangement[j]
            if not (int2[1]>=int1[0] and int2[0]<=int1[1]):
                overlap=False
        if overlap:
            overlappingarrs+=1
        if overlappingarrs==2:
            break
    if overlappingarrs>=2:
        overlappingarrs=0
        #print(arrangement)
        valid+=1
print(valid,len(pairingslist))
print(float(valid)/len(pairingslist))
#answer is 0.4 for N>4
        
        
 