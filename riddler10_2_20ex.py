#riddler express
def listDifferencesOfPerfectSquares():
    divisors=[1, 2, 5, 7, 10, 14, 25, 35, 50, 70, 175, 350]
    for i in range(len(divisors)-1,int(len(divisors)/2)-1,-1):
        m=divisors[i]
        N=350/m*2
        M=m*2
        A=(M+N)/2
        B=(M-N)/2
        assert(A**2-B**2==1400)
        print(int(A),"^2 -",int(B),"^2 =",int(A**2-B**2))
listDifferencesOfPerfectSquares()