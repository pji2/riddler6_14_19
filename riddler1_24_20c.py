#Riddler classic 1-24-2020

#memoization
StoredStates=dict()        

def getUniques(nextmoves):
    uniques=[]
    for pair in nextmoves:
        s=tuple(sorted(pair))
        if s not in uniques:
            uniques.append(s)
    return uniques

#Is this a winning move for the next player?
def getStateStatus(pair):
    #base cases
    if pair == (0,0):
        StoredStates[pair]=False
        return False
    elif min(list(pair))==0:
        StoredStates[pair]=True
        return True
    elif min(list(pair))>0 and pair[0]==pair[1]:
        StoredStates[pair]=True
        return True
    
    #try all possible moves
    nextpairs=[]
    minpair=min(list(pair))
    for i in range(1,minpair):
        nexttuple=(pair[0]-i,pair[1]-i)
        nextpairs.append(nexttuple)
    for i in range(1,pair[0]):
        nexttuple=(pair[0]-i,pair[1])
        nextpairs.append(nexttuple)
    for i in range(1,pair[1]):
        nexttuple=(pair[0],pair[1]-i)
        nextpairs.append(nexttuple)
        
    #consider sorted pairs only
    nextpairs=getUniques(nextpairs)
    statuses=[]
    winning=False
    for p in nextpairs:
        #any position that goes to a losing position is a winning one
        if p in StoredStates:
            if StoredStates[p]==False:
                winning=True
                break
        elif getStateStatus(p)==False:
            winning=True
            break
    #else if they all go to winning positions, this position is losing
    StoredStates[pair]=winning
    return winning

winningnums=[]
for sum_ in range(20,31):
    sum_all_winning=True
    for i in range(1,int(sum_/2)+1):
        x=i;y=sum_-i
        if getStateStatus((x,y))==False:
            print("found losing position: ",x,y)
            sum_all_winning=False
            break
    if sum_all_winning:
        winningnums.append(sum_)
        
print("Player that does NOT divide the pennies wins with these number of pennies: ",winningnums)

#Output:
"""

found losing position:  8 13
found losing position:  9 15
found losing position:  11 18
Player who does NOT divide the pennies wins with these numbers of pennies:  [20, 22, 23, 25, 26, 27, 28, 30]
"""

