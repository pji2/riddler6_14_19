#The array for a given position represents the number of each kind of card we have remaining
mem=dict()
def points_stop(state):
    points=0
    for i in range(len(state)):
        if state[i]==0:
            points+=(i+1)
    return points

def points_continue(state):
    points_expected=0
    num_future_states=sum(state)+1 #+1 for Joker card
    for i in range(len(state)):
        if state[i]:
            future_state=state[:]
            future_state[i]=0
            points_expected+=1.0/num_future_states*optimal_points(future_state)
    return points_expected
            
def optimal_points(state):
    if sum(state)==0:
        return sum([x+1 for x in range(len(state))])
    if tuple(state) in mem:
        return mem[tuple(state)]
    a=points_stop(state)
    b=points_continue(state)
    if tuple(state) not in mem:
        mem[tuple(state)]=max(a,b)
    """if a>=b:
        print(state,",stop,",a,",",b)
    else:
        print(state,",continue,",a,",",b)"""
    return max(a,b)

print(optimal_points([1,1,1,1,1,1,1,1,1,1]))

"""
Output:
15.453102453102451
"""