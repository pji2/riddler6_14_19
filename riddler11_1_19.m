%test various cutoff points across all permutations
possibles = perms([1 2 3 4 5 6 7 8 9 10]);
expectedvalues=[5.5 7.7 8.0667 7.9750 7.7 7.3333 6.9143 6.4625 5.9889 5.5]; 
for cutoff=9:9 %find one cutoff point at a time and insert manually into expectedvalues
    e=0;
    for p=1:factorial(10)
        perm=possibles(p,:);
        max_before_cutoff=max(perm(1:cutoff-1));
        greater=perm(cutoff:10)>ones(1,10-cutoff+1)*max_before_cutoff;
        idx = find(greater~=0, 1, 'first');
        if isempty(idx)
            assert(max_before_cutoff==10)
            next_greater=perm(10);
        else
            next_greater=perm(cutoff+idx-1);
        end
        e=e+next_greater;
    end
    expectedvalues(cutoff)=e/factorial(10);
end
expectedvalues
actualranks_for_cutoff_values=10-expectedvalues+1
%acutalranks=[5.5 3.3 2.9333 3.0250 3.3 3.6667 4.0857 4.5375 5.0111 5.5]
%2.9333 is expected rank of chosen suitor
best_rank=max(actualranks_for_cutoff_values)

%analytical solution: https://arxiv.org/abs/1605.06478
%10=highest in equations and code
n=10;
op_c=floor(sqrt(n+1/4)+1/2);
un=5.5;
s=0;
c=op_c;
for k=c:n-1
    s=s+k*(n+1)/(k+1)*(c-1)/(k-1)*1/k;
end
s=s+(c-1)/(n-1)*un;
best_rank_analytical=10-s+1

