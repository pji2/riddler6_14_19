grid=[[0 for x in range(8)] for x in range(3)]
triplets=[[(6,7,7),(7,6,7),(7,7,6)],[(6,6,6),(3,8,9),(3,9,8),(8,9,3),(8,3,9),(9,3,8),(9,8,3),(4,6,9),(4,9,6),(6,9,4),\
                                     (6,4,9),(9,4,6),(9,6,4)],[(5,3,9),(5,9,3),(3,9,5),(3,5,9),(9,3,5),(9,5,3)],\
          [(2,7,7),(7,2,7),(7,7,2)],[(7,8,2),(7,2,8),(2,8,7),(2,7,8),(8,2,7),(8,7,2),(7,4,4),(4,7,4),(4,4,7)],\
          [(7,4,3),(7,3,4),(3,4,7),(3,7,4),(4,3,7),(4,7,3),(7,6,2),(7,2,6),(6,2,7),(6,7,2),(2,6,7),(2,7,6)],\
          [(7,7,5),(7,5,7),(5,7,7)],[(5,8,1),(5,1,8),(1,5,8),(1,8,5),(8,1,5),(8,5,1),(5,4,2),(5,2,4),(2,4,5),\
                                    (2,5,4),(4,2,5),(4,5,2)]]

from functools import reduce
def success(grid):
    col1=grid[0];col2=grid[1];col3=grid[2]
    result1 = reduce((lambda x, y: x * y), col1)
    result2 = reduce((lambda x, y: x * y), col2)
    result3 = reduce((lambda x, y: x * y), col3)
    if col1.count(7)>3 or col1.count(5)>1 or col2.count(7)>2 or col2.count(5)>2 or col3.count(7)>3:
        return False
    if result1>8890560 or result2>156800 or result3>55566:
        return False
    if result1==8890560 and result2==156800 and result3==55566:
        return True
    return False
    
def solve(grid,triplets):
    sample=grid[0]
    if sample.count(0)==0:
        return success(grid)
    row=sample.index(0)
    placements=triplets[row]
    for p in range(len(placements)):
        grid[0][row]=placements[p][0]
        grid[1][row]=placements[p][1]
        grid[2][row]=placements[p][2]
        if solve(grid,triplets):
            print(grid)
            return True
        else:
            grid[0][row]=0
            grid[1][row]=0
            grid[2][row]=0
    return False
                
            
print(solve(grid,triplets))

#Output:
"""
[[7, 9, 9, 7, 8, 7, 5, 8], [7, 8, 5, 2, 2, 4, 7, 5], [6, 3, 3, 7, 7, 3, 7, 1]]
[[7, 9, 9, 7, 8, 7, 5, 8], [7, 8, 5, 2, 2, 4, 7, 5], [6, 3, 3, 7, 7, 3, 7, 1]]
[[7, 9, 9, 7, 8, 7, 5, 8], [7, 8, 5, 2, 2, 4, 7, 5], [6, 3, 3, 7, 7, 3, 7, 1]]
[[7, 9, 9, 7, 8, 7, 5, 8], [7, 8, 5, 2, 2, 4, 7, 5], [6, 3, 3, 7, 7, 3, 7, 1]]
[[7, 9, 9, 7, 8, 7, 5, 8], [7, 8, 5, 2, 2, 4, 7, 5], [6, 3, 3, 7, 7, 3, 7, 1]]
[[7, 9, 9, 7, 8, 7, 5, 8], [7, 8, 5, 2, 2, 4, 7, 5], [6, 3, 3, 7, 7, 3, 7, 1]]
[[7, 9, 9, 7, 8, 7, 5, 8], [7, 8, 5, 2, 2, 4, 7, 5], [6, 3, 3, 7, 7, 3, 7, 1]]
[[7, 9, 9, 7, 8, 7, 5, 8], [7, 8, 5, 2, 2, 4, 7, 5], [6, 3, 3, 7, 7, 3, 7, 1]]
True
"""