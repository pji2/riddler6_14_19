#triangle point test from https://blackpawn.com/texts/pointinpoly/#:~:text=A%20common%20way%20to%20check%20if%20a%20point,not.%20It%20works%2C%20but%20it%20is%20very%20slow.
"""
This simulation supports the analytical answer of 46.2097%:
See https://bitbucket.org/pji2/riddler6_14_19/src/master/riddler10_22_21.jpeg
https://bitbucket.org/pji2/riddler6_14_19/src/master/riddler10_22_21_2.jpeg
"""

import numpy as np, math, random

def CrossProduct(a,b):
    return np.cross(a,b)

def DotProduct(a,b):
    return np.dot(a,b)


def SameSide(p1,p2, a,b):
    cp1 = CrossProduct(np.subtract(b,a), np.subtract(p1,a))
    cp2 = CrossProduct(np.subtract(b,a), np.subtract(p2,a))
    if DotProduct(cp1, cp2) >= 0:
        return True
    return False

def PointInTriangle(p, a,b,c):
    if SameSide(p,a, b,c) and SameSide(p,b, a,c) and SameSide(p,c, a,b):
            return True
    return False
    
vertexA=[-.5,-math.sqrt(3)/6]
vertexB=[0,math.sqrt(3)/3]
vertexC=[.5,-math.sqrt(3)/6]

c=0
for n in range(100000):
    t1=random.random()
    t2=random.random()
    t3=random.random()
    randtA=(np.add(vertexA,np.subtract(vertexB,vertexA)*t1))
    randtB=(np.add(vertexB,np.subtract(vertexC,vertexB)*t2))
    randtC=(np.add(vertexC,np.subtract(vertexA,vertexC)*t3))
    #print(randtA,randtB,randtC)
    if PointInTriangle([0,0],randtA,randtB,randtC):
        c+=1
print(c/100000.0)

"""Sample result:
0.46116
"""