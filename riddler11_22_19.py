from __future__ import print_function

import collections

import sys

import csv

# Import Python wrapper for or-tools CP-SAT solver.
from ortools.sat.python import cp_model


def getfactorizations():
    bank=dict()
    with open('riddler11_22_19.csv', 'r') as csvFile:
        reader = csv.reader(csvFile)
        rownum=0
        lastkey=""
        lastrow=31
        for row in reader:
            if rownum>lastrow:
                break
            if rownum>=1:
                key=row[0]+'/'+row[1]+'/'+row[2]+'/'+row[3]+'/'+row[4]+'/'+row[5]
                bank[row[0]]=[row[1],row[2],row[3],row[4],row[5]]
            rownum+=1
    csvFile.close()
    return bank

def findLottery():
    #list of valid composite numbers with at least 2 distinct factors 1-70
    numbers=[6,10,12,14,15,18,20,21,22,24,28,30,33,35,36,40,42,44,45,48,50,54,55,56,60,63,66,70]
    bank=getfactorizations()
    assert(len(numbers)==len(bank))
    players=[1,2,3,4,5]
    model = cp_model.CpModel()
    choices={}
    
    #create variables
    for p in players:
        for n in numbers:
            label=str(p)+'/'+str(n)
            choices[p,n]=model.NewIntVar(0,1,label) #is this number assigned to this player?
            
            
    #constraints: each number can't be assigned to multiple players
    for n in numbers:
        model.Add(sum(choices[p,n] for p in players) <= 1)
        
    #each person must select 5 numbers 
    for p in players:
        model.Add(sum(choices[p,n] for n in numbers) == 5)
        
        
    #constraint: all players have the same number of factors of 2,3,5,7,11 in their numbers

    model.Add(sum(choices[1,n]*int(bank[str(n)][0]) for n in numbers)==sum(choices[2,n]*int(bank[str(n)][0]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][1]) for n in numbers)==sum(choices[2,n]*int(bank[str(n)][1]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][2]) for n in numbers)==sum(choices[2,n]*int(bank[str(n)][2]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][3]) for n in numbers)==sum(choices[2,n]*int(bank[str(n)][3]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][4]) for n in numbers)==sum(choices[2,n]*int(bank[str(n)][4]) for n in numbers))
    
    model.Add(sum(choices[1,n]*int(bank[str(n)][0]) for n in numbers)==sum(choices[3,n]*int(bank[str(n)][0]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][1]) for n in numbers)==sum(choices[3,n]*int(bank[str(n)][1]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][2]) for n in numbers)==sum(choices[3,n]*int(bank[str(n)][2]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][3]) for n in numbers)==sum(choices[3,n]*int(bank[str(n)][3]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][4]) for n in numbers)==sum(choices[3,n]*int(bank[str(n)][4]) for n in numbers))
    
    model.Add(sum(choices[1,n]*int(bank[str(n)][0]) for n in numbers)==sum(choices[4,n]*int(bank[str(n)][0]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][1]) for n in numbers)==sum(choices[4,n]*int(bank[str(n)][1]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][2]) for n in numbers)==sum(choices[4,n]*int(bank[str(n)][2]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][3]) for n in numbers)==sum(choices[4,n]*int(bank[str(n)][3]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][4]) for n in numbers)==sum(choices[4,n]*int(bank[str(n)][4]) for n in numbers))
    
    model.Add(sum(choices[1,n]*int(bank[str(n)][0]) for n in numbers)==sum(choices[5,n]*int(bank[str(n)][0]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][1]) for n in numbers)==sum(choices[5,n]*int(bank[str(n)][1]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][2]) for n in numbers)==sum(choices[5,n]*int(bank[str(n)][2]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][3]) for n in numbers)==sum(choices[5,n]*int(bank[str(n)][3]) for n in numbers))
    model.Add(sum(choices[1,n]*int(bank[str(n)][4]) for n in numbers)==sum(choices[5,n]*int(bank[str(n)][4]) for n in numbers))
    
    #solve model
    solver = cp_model.CpSolver()

    status = solver.Solve(model)
    print(status)
    if status==cp_model.FEASIBLE:
        product=1
        for p in players:
            for n in numbers:
                if solver.Value(choices[p,n]):
                    if p==1:
                        product*=n
                    print("player ",p," selects ",n)
    print("product is: ",product)
findLottery()

"""
Output:

2
player  1  selects  15
player  1  selects  18
player  1  selects  40
player  1  selects  42
player  1  selects  44
player  2  selects  12
player  2  selects  24
player  2  selects  28
player  2  selects  45
player  2  selects  55
player  3  selects  6
player  3  selects  22
player  3  selects  50
player  3  selects  54
player  3  selects  56
player  4  selects  10
player  4  selects  21
player  4  selects  33
player  4  selects  48
player  4  selects  60
player  5  selects  14
player  5  selects  20
player  5  selects  30
player  5  selects  36
player  5  selects  66
product is:  19958400
"""