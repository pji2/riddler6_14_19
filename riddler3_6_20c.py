#Classic
Decision_mat={} #stores the choice of how many tiles to attempt to eliminate
Prob_mat={}

def winprob(N,M):
    for t in range(N+M-1,0,-1):
        for n in range(1,N+1):
            for m in range(1,M+1):
                if n==1:
                    Prob_mat[(t,n,m)] = 1.0
                elif t==N+M-1:
                    Prob_mat[(t,n,m)] = 0.0
                else:
                    bestj=[]
                    for j in range(1,n): #ask a question for 1 to n-1 characters
                        winprob_j= float(j)/n * (1-Prob_mat[(t+1,m,j)]) + (1-float(j)/n)*(1-Prob_mat[(t+1,m,n-j)])
                        bestj.append(winprob_j)
                    random=1.0/n
                    if random >= max(bestj):
                        Decision_mat[(t,n,m)] = "choose random"
                    Prob_mat[(t,n,m)]=max(random,max(bestj))
                    Decision_mat[(t,n,m)]=bestj.index(max(bestj))+1
    #print(Prob_mat)
    #print(Decision_mat)
    return Prob_mat[(1,N,M)]

print("4 tiles each: ",winprob(4,4))
print("14 tiles each: ",winprob(14,14))
print("24 tiles each: ",winprob(24,24))