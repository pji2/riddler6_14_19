from itertools import combinations
sections=[1.0,.81,.64,.49,.36,.25,.16,.09,.04,.01,0]
probs=[]
for j in range(10):
    probs.append(sections[j]-sections[j+1])
inds=[0,1,2,3,4,5,6,7,8,9]
CumDist=[]
for consec in range(1,11):
    P=0
    comb=combinations(inds,consec)
    for i in list(comb):
        p=1.0
        for j in i:
            p*=probs[j]
        P+=p
    CumDist.append(P)
Expected=0
for j in range(len(CumDist)-1):
    Expected+=(j+2)*(CumDist[j]-CumDist[j+1])
print("Answer:",Expected)

#Output
#Answer: 2.558528499404354