from scipy.optimize import fsolve
import math

def equations(p,s):
    d,k = p
    f1=d*math.cos(s)-k*(math.cos(s)**2+1)*d**2+math.sin(s)-math.sin(s+d)
    f2=math.cos(s)-2*k*d*(math.cos(s)**2+1)-math.cos(s+d)
    return (f1,f2)

def parabola(k,s,xsub):
    y=(xsub-s)*math.cos(s)-k*(math.cos(s)**2+1)*(xsub-s)**2+math.sin(s)
    return y

def pathclear(k,s,d):
    pathclear=True
    for x_ in range(int((s+.005)*1000),int((s+d-.005)*1000)):
        x=x_/1000.0
        if parabola(k,s,x)-math.sin(x)<0:
            pathclear=False
    return pathclear
maxd=0
maxs=0
maxk=0
for s_ in range(0,int(math.pi/2*1000)):
    s=s_/1000.0
    d=0
    k=0
    #test various initial conditions for d to find best d
    for initd in range(int(2*math.pi*100),int(4*math.pi*100),50):
        init_k=5/100.0
        init_d=initd/100.0
        d_, k_ =  fsolve(equations,(init_d,init_k),args=s)
        if d_>.01 and d_>d and k_>0:
            d=d_
            k=k_
    #if d>.01:
    #    print(s,d,k,2*math.pi,4*math.pi,d/(2*math.pi),pathclear(k,s,d))
    if d>maxd and d<=4*math.pi and k>0 and pathclear(k,s,d):
        maxd=d
        maxs=s
        maxk=k
print("launch point:",maxs,"fly distance:",maxd,"constant:",maxk)
print("number of hills:",maxd/(2*math.pi))


from sympy import *
from sympy.plotting import plot

x = symbols('x')
y=(x-maxs)*cos(maxs)-maxk*(cos(maxs)**2+1)*(x-maxs)**2+sin(maxs)
plot(y,sin(x),(x,-1,maxs+maxd+1),xlim=(-1,maxs+maxd+1),ylim=(-1,5))
plot(y,sin(x),(x,-1,maxs+maxd+1),xlim=(0,2),ylim=(0,2))

"""Output:
launch point: 0.218 fly distance: 8.988777967084111 constant: 0.055608960898561904
number of hills: 1.4306084458169543
"""