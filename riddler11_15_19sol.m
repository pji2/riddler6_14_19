En_s=zeros(1,10); %holds the expected value of starting with n=0:9
for n=1:9 %find expected values, starting with 1. En(0)=0
    E_n=0;
    for l=0:n-1 %consider lower digits
        for r=0:20 %consider this digit repeated 0:20 times
            lower_prob=1/n; %probability that you get l<n is 1/n
            repeated_prob=(1/(n+1))^r*n/(n+1); %probability that you get r repeats
            prob=repeated_prob*lower_prob; %joint probability
            %the expected value of getting this lower digit, 
            %and shifted over by r repeats of the current digit
            lower_val = .1*En_s(l+1)*10^-r;
            %value of this digit repeated r+1 times (need at least 1 'repeat')
            repeated_val = n*1/9*(1-10^-(r+1));
            %sum over expected values
            E_n=E_n+(lower_val+repeated_val)*prob;
        end
    end
    En_s(n+1)=E_n; %set up induction
end
En_s
mean(En_s)