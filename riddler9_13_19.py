
# coding: utf-8

# In[3]:


states=["AL","AK","AS","AZ","AR","CA","CO","CT","DE","DC","FM","FL","GA","GU","HI","ID","IL","IN","IA","KS","KY","LA","ME","MH","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ",       "NM","NY","NC","ND","MP","OH","OK","OR","PW","PA","PR","RI","SC","SD","TN","TX","UT","VT","VI","VA","WA","WV","WI","WY"]#,"AE","AP","AA"]

#returns a dictionary with keys as states and values as states that can follow
def genDict(states):
    dic=dict()
    for s in states:
        thisvalue=[]
        for i in range(len(states)):
            if states[i]!=s and s[1]==states[i][0]:
                thisvalue.append(states[i])
        dic[s]=thisvalue
    return dic

#create the dictionary
dic = genDict(states)
print(len(dic))
#global variables to keep track of maximum length string
MaxLen=0
Finalstring=""

#main function
def maxLen(currentstate, dic_avail, numstates, string):
    global MaxLen
    global Finalstring
    if numstates>MaxLen:
        MaxLen=numstates
        Finalstring=string
    for nextstate in dic_avail[currentstate]:
        if nextstate in dic_avail.keys():
            values=dic_avail[currentstate]
            del dic_avail[currentstate]
            maxLen(nextstate, dic_avail, numstates+1, string+nextstate[1])
            dic_avail[currentstate]=values

maxLen("AL",dic,1,"AL")
print(MaxLen,Finalstring)

