#Turn all absolute values into paired parenthesis, then set () as the absolute value operator.
#Then, list all possible ways of ordering (N+1)/2 pairs of parenthesis
validParens=[]
def printallvalidParens(nopen,nclosed,string):
    if nopen==0 and nclosed==0:
        validParens.append(string)
    elif nopen>nclosed:
        return
    if nopen>0:
        printallvalidParens(nopen-1,nclosed,string+'(')
    if nclosed>0:
        printallvalidParens(nopen,nclosed-1,string+')')

#evaluate this possible intepretation of the expression by converting it into string and using
#python's eval() function
def evaluate(expr):
    newexpr=""
    for i in range(len(expr)):
        e=expr[i]
        if i==0:
            newexpr+="abs("
        elif e=='-' or e==')':
            newexpr+=e
        elif e in "1234567890":
            newexpr+=e
        elif e=='(':
            newexpr+="*abs("
    print(newexpr," = ",eval(newexpr))
    return eval(newexpr)
        
#Get the distinct ways of evaluating |-1|-2|...|-N|
def getdistinctvals(N):
    distinct_values=[]
    expr="|"
    for i in range(1,N+1):
        expr+=str(-i)+'|'
    print("evaluate: ",expr)
    
    #List all possible ways of pairing absolute value signs, now denoted by ()
    printallvalidParens(int((N+1)/2),int((N+1)/2),"")
    #print(len(validParens), "ways to evaluate absolute value signs")
    
    #set possible pairing into expression
    for i in range(len(validParens)):
        val=validParens[i]
        ele=0
        newstr=""
        for e in range(len(expr)):
            if expr[e]=='|':
                newstr+=val[ele]
                ele+=1
            else:
                newstr+=expr[e]
        #evaluate expression
        value = evaluate(newstr)
        distinct_values.append(value)
    return set(distinct_values)
    
valueset = getdistinctvals(9)
print(len(valueset), " distinct values")

"""Output:

evaluate:  |-1|-2|-3|-4|-5|-6|-7|-8|-9|
abs(-1*abs(-2*abs(-3*abs(-4*abs(-5)-6)-7)-8)-9)  =  187
abs(-1*abs(-2*abs(-3*abs(-4)-5*abs(-6)-7)-8)-9)  =  115
abs(-1*abs(-2*abs(-3*abs(-4)-5)-6*abs(-7)-8)-9)  =  93
abs(-1*abs(-2*abs(-3*abs(-4)-5)-6)-7*abs(-8)-9)  =  105
abs(-1*abs(-2*abs(-3*abs(-4)-5)-6)-7)-8*abs(-9)  =  -25
abs(-1*abs(-2*abs(-3)-4*abs(-5*abs(-6)-7)-8)-9)  =  171
abs(-1*abs(-2*abs(-3)-4*abs(-5)-6*abs(-7)-8)-9)  =  85
abs(-1*abs(-2*abs(-3)-4*abs(-5)-6)-7*abs(-8)-9)  =  97
abs(-1*abs(-2*abs(-3)-4*abs(-5)-6)-7)-8*abs(-9)  =  -33
abs(-1*abs(-2*abs(-3)-4)-5*abs(-6*abs(-7)-8)-9)  =  269
abs(-1*abs(-2*abs(-3)-4)-5*abs(-6)-7*abs(-8)-9)  =  105
abs(-1*abs(-2*abs(-3)-4)-5*abs(-6)-7)-8*abs(-9)  =  -25
abs(-1*abs(-2*abs(-3)-4)-5)-6*abs(-7*abs(-8)-9)  =  -375
abs(-1*abs(-2*abs(-3)-4)-5)-6*abs(-7)-8*abs(-9)  =  -99
abs(-1*abs(-2)-3*abs(-4*abs(-5*abs(-6)-7)-8)-9)  =  479
abs(-1*abs(-2)-3*abs(-4*abs(-5)-6*abs(-7)-8)-9)  =  221
abs(-1*abs(-2)-3*abs(-4*abs(-5)-6)-7*abs(-8)-9)  =  145
abs(-1*abs(-2)-3*abs(-4*abs(-5)-6)-7)-8*abs(-9)  =  15
abs(-1*abs(-2)-3*abs(-4)-5*abs(-6*abs(-7)-8)-9)  =  273
abs(-1*abs(-2)-3*abs(-4)-5*abs(-6)-7*abs(-8)-9)  =  109
abs(-1*abs(-2)-3*abs(-4)-5*abs(-6)-7)-8*abs(-9)  =  -21
abs(-1*abs(-2)-3*abs(-4)-5)-6*abs(-7*abs(-8)-9)  =  -371
abs(-1*abs(-2)-3*abs(-4)-5)-6*abs(-7)-8*abs(-9)  =  -95
abs(-1*abs(-2)-3)-4*abs(-5*abs(-6*abs(-7)-8)-9)  =  -1031
abs(-1*abs(-2)-3)-4*abs(-5*abs(-6)-7*abs(-8)-9)  =  -375
abs(-1*abs(-2)-3)-4*abs(-5*abs(-6)-7)-8*abs(-9)  =  -215
abs(-1*abs(-2)-3)-4*abs(-5)-6*abs(-7*abs(-8)-9)  =  -405
abs(-1*abs(-2)-3)-4*abs(-5)-6*abs(-7)-8*abs(-9)  =  -129
abs(-1)-2*abs(-3*abs(-4*abs(-5*abs(-6)-7)-8)-9)  =  -953
abs(-1)-2*abs(-3*abs(-4*abs(-5)-6*abs(-7)-8)-9)  =  -437
abs(-1)-2*abs(-3*abs(-4*abs(-5)-6)-7*abs(-8)-9)  =  -285
abs(-1)-2*abs(-3*abs(-4*abs(-5)-6)-7)-8*abs(-9)  =  -241
abs(-1)-2*abs(-3*abs(-4)-5*abs(-6*abs(-7)-8)-9)  =  -541
abs(-1)-2*abs(-3*abs(-4)-5*abs(-6)-7*abs(-8)-9)  =  -213
abs(-1)-2*abs(-3*abs(-4)-5*abs(-6)-7)-8*abs(-9)  =  -169
abs(-1)-2*abs(-3*abs(-4)-5)-6*abs(-7*abs(-8)-9)  =  -423
abs(-1)-2*abs(-3*abs(-4)-5)-6*abs(-7)-8*abs(-9)  =  -147
abs(-1)-2*abs(-3)-4*abs(-5*abs(-6*abs(-7)-8)-9)  =  -1041
abs(-1)-2*abs(-3)-4*abs(-5*abs(-6)-7*abs(-8)-9)  =  -385
abs(-1)-2*abs(-3)-4*abs(-5*abs(-6)-7)-8*abs(-9)  =  -225
abs(-1)-2*abs(-3)-4*abs(-5)-6*abs(-7*abs(-8)-9)  =  -415
abs(-1)-2*abs(-3)-4*abs(-5)-6*abs(-7)-8*abs(-9)  =  -139
39  distinct values
"""
