#find the pairings that cross each other
N=11
constrain_crossings=[]
c=0
for a1 in range(N):
    for a2 in range(a1+2,a1+N-1):
        for b1 in range(a1+1,a2):
            for b2 in range(a2+1,a1+N):
                if a1%N<a2%N and b1%N<b2%N:
                    constrain_crossings.append((a1%N,a2%N,b1%N,b2%N))
                    c+=1
                    

from ortools.sat.python import cp_model

#LIP solution to a given arrangement
def findMax(sequence):
    #sequence=[1,4,8,7,11,2,5,9,3,6,10]
    model = cp_model.CpModel()
    N=len(sequence)
    pairings=[[0 for x in range(N)] for y in range(N)]
    for i in range(N):
        for j in range(i+1,N):
            #pairings[i][j]=1
            pairings[i][j]=model.NewIntVar(0,1,str(i)+","+str(j))
    #print(pairings)
    for s in constrain_crossings:
        model.Add(pairings[s[0]][s[1]]+pairings[s[2]][s[3]]<=1)

    for i in range(N):
        model.Add(sum(pairings[i][j]+pairings[j][i] for j in range(N))<=1)

    obj=0
    for i in range(N):
        for j in range(i+1,N):
            obj+=sequence[i]*sequence[j]*pairings[i][j]
    model.Maximize(obj)
    solver = cp_model.CpSolver()
    status = solver.Solve(model)
    if status == cp_model.OPTIMAL:
        total=0
        seq=""
        for i in range(N):
            for j in range(i+1,N):
                if solver.Value(pairings[i][j]):
                    #print(str(sequence[i])+","+str(sequence[j]))
                    total+=sequence[i]*sequence[j]
                    seq+="("+str(sequence[i])+","+str(sequence[j])+")"
    return total, seq

print(findMax([1, 4, 2, 3, 9, 6, 10 , 7, 11, 5, 8]))

"""
Output:
(224, '(4,3)(9,8)(6,5)(10,11)')
"""