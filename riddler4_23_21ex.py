import math
def dist(p1,p2):
    x1=p1[0]
    y1=p1[1]
    x2=p2[0]
    y2=p2[1]
    return math.sqrt((x2-x1)**2+(y2-y1)**2)

def TriangleArea(p1,p2,p3):
    s1=dist(p1,p2)
    s2=dist(p1,p3)
    s3=dist(p2,p3)
    s=(s1+s2+s3)/2.0
    return math.sqrt(s*(s-s1)*(s-s2)*(s-s3))

maxA=0
maxangle=0
for angle in range(1,90):
    theta=angle*math.pi/180.0
    T1=(-(1-math.sin(theta))*math.tan(theta)+math.cos(theta),1)
    T2=(1,(math.cos(theta)-1)/math.tan(theta)+math.sin(theta))
    T3=(1,1)
    Area=TriangleArea(T1,T2,T3)
    if Area>maxA:
        maxA=Area
        maxangle=angle
print("angle, area:", maxangle,maxA)
A1=maxA

maxA=0
maxangle1=0
maxangel2=0
for angle1 in range(1,45):
    for angle2 in range(45,90):
        theta1=angle1*math.pi/180.0
        theta2=angle2*math.pi/180.0
        P1=(-(1-math.sin(theta2))*math.tan(theta2)+math.cos(theta2),1)
        P2=(-(1-math.sin(theta1))*math.tan(theta1)+math.cos(theta1),1)
        P3=(math.cos((theta2+theta1)/2)/math.cos((theta2-theta1)/2),math.sin((theta2+theta1)/2)/math.cos((theta2-theta1)/2))
        P4=(1,1)
        P5=(1,(math.cos(theta1)-1)/math.tan(theta1)+math.sin(theta1))
        Area1=TriangleArea(P1,P2,P3)
        Area2=TriangleArea(P2,P4,P5)
        if Area1+Area2>maxA:
            maxA=Area1+Area2
            maxangle1=angle1
            maxangle2=angle2
print("angles, area:",maxangle1,maxangle2,maxA)
A2=maxA
print("Area remaining", 4-(3*A1+A2))
"""
Output:
angle, area: 45 0.1715728752538099
angles, area: 30 60 0.19615242270663177
3.2891289515319384
"""