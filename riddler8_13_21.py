from itertools import product

results=dict()
results[1]=3.5
results[0]=0
def expectedmaxscore(N,fixedblocks):
    remaining=N-len(fixedblocks)
    if remaining in results:
        return results[remaining]
    dicerolls=list(product(range(1,7), repeat=remaining))
    #print(dicerolls)
    expected=0
    for d in dicerolls:
        d=sorted(list(d),reverse=True)
        bestoption=[]
        for numfix in range(1,remaining+1):
            fixedblocks_c=fixedblocks[:]
            fixedblocks_c.extend(d[:numfix])
            score=sum(d[:numfix])+expectedmaxscore(N,fixedblocks_c)
            bestoption.append(score)
        expected+=1.0/6**remaining*max(bestoption)
    results[remaining]=expected
    return expected

print(expectedmaxscore(4,[]))

"""Output:
18.84364045115052
"""