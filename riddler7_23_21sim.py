import random
def createintervals(oldcuts,newcut,intervals):
    for i in range(len(oldcuts)-1):
        if newcut>=oldcuts[i] and newcut<oldcuts[i+1]:
            upperinterval=oldcuts[i+1]-newcut
            if i==0:
                lowerinterval=newcut
            else:
                lowerinterval=newcut-oldcuts[i]
            break
    oldcuts.insert(i+1,newcut)
    intervals.insert(i,lowerinterval)
    intervals[i+1]=upperinterval
    return oldcuts,intervals
   
total=10000
ave=0
for iters in range(total):
    oldcuts=[0,1]
    intervals=[100]
    n=0
    while max(intervals)>0.1:
        newcut=random.random()
        cuts,intervals=createintervals(oldcuts,newcut,intervals)
        n+=1
        #print(newcut,cuts,intervals)
    #print(n)
    ave+=n/10000.0
print(ave)

#Output: 42.89030000000027


#Extra Credit
from collections import defaultdict
  
import math

def dist(u,v):
    return math.sqrt((v[0]-u[0])**2+(v[1]-u[1])**2)

# program to check if there is exist a path between two vertices
# of a graph adapted from https://www.geeksforgeeks.org/find-if-there-is-a-path-between-two-vertices-in-a-given-graph/

#This class represents a directed graph using adjacency list representation
class Graph:
  
    def __init__(self,vertices):
        self.V= vertices #No. of vertices
        self.nodecoords=["top","bottom"] #bottom, top of wall
        self.graph = defaultdict(list) # default dictionary to store graph
        #print(self.graph)
  
    # function to add an edge to graph
    def addEdge(self,u,v):
        self.graph[u].append(v)
    
    def addVertex(self,u):
        self.nodecoords.append((u[0],u[1]))
        for i in range(2,self.V):
            if dist(u,self.nodecoords[i])<=0.1:
                self.addEdge(self.V,i)
                self.addEdge(i,self.V)
        if u[1]<=0.1:
            self.addEdge(self.V,0)
            self.addEdge(0,self.V)
        if u[1]>=0.9:
            self.addEdge(self.V,1)
            self.addEdge(1,self.V)
            
        self.V+=1
        #print(self.nodecoords,self.graph)
        
      
     # Use BFS to check path between s and d
    def isReachable(self, s, d):
        # Mark all the vertices as not visited
        visited =[False]*(self.V)
  
        # Create a queue for BFS
        queue=[]
  
        # Mark the source node as visited and enqueue it
        queue.append(s)
        visited[s] = True
  
        while queue:
 
            #Dequeue a vertex from queue
            n = queue.pop(0)
             
            # If this adjacent node is the destination node,
            # then return true
            if n == d:
                return True
 
            #  Else, continue to do BFS
            for i in self.graph[n]:
                if visited[i] == False:
                    queue.append(i)
                    visited[i] = True
         # If BFS is complete without visited d
        return False
        
# Create a graph given in the above diagram

ave=0
for n in range(10000):
    g = Graph(2)

    points=0
    while not g.isReachable(0,1):
        points+=1
        xcoord=random.random()
        ycoord=random.random()
        g.addVertex((xcoord,ycoord))
    ave+=points/10000.0
print(ave)

#Output: 143.0543000000004