import numpy as np
import math
from scipy.integrate import odeint
import matplotlib.pyplot as plt

def drdtpositive(r,t):
    #sqrt can't be <0, so limit to 0
    drdt=2*np.sqrt(max(0,1-2500*r**2/(t**4-400*t**3+80000*t**2-8000000*t+400000000)))
    return drdt

def drdtnegative(r,t):
    drdt=-2*np.sqrt(1-2500*r**2/(t**4-400*t**3+80000*t**2-8000000*t+400000000))
    return drdt
    
r0=100*np.sqrt(2)
# time points
t=np.linspace(0,200,1000)
thetas=[]
for ti in t:
    theta=math.atan(100/(100-ti))
    thetas.append(theta)

# solve ODE
R=odeint(drdtpositive,r0,t)
R2=odeint(drdtnegative,r0,t)

print("final distance", R[-1])
# plot results
plt.plot(t,R)
plt.xlabel('time')
plt.ylabel('R(t)')
plt.show()

plt.plot(t,thetas)
plt.xlabel('time')
plt.ylabel('theta(t)')
plt.show()

xs=[]
ys=[]
for i in range(len(t)):
    ti=t[i]
    Ri=R[i]
    theta=math.atan2(100,100-ti)
    x=Ri*math.cos(theta)
    y=Ri*math.sin(theta)
    xs.append(x)
    ys.append(y)

t_p=np.linspace(-100,100,2)
plt.plot(0,0,'o')
plt.plot(t_p,[-100,-100])
plt.plot(xs,ys)
plt.xlim(-250,250)
plt.ylim(-250,250)
plt.gca().set_aspect('equal', adjustable='box')
plt.show()

"""Output:
final distance [288.96011988]
(+3 images see https://bitbucket.org/pji2/riddler6_14_19/src/master/riddler10_8_21.pdf)
"""