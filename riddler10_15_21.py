Threshold=2
#Express
allseries=dict()
for i in range(2**7):
    series=str(bin(i)[2:])
    series=series.zfill(7)
    for k in range(4,8):
        series=[int(s) for s in series]
        if sum(series[:k])==4 or series[:k].count(0)==4:
            if tuple(series[:k]) not in allseries:
                allseries[tuple(series[:k])]=1.0/2**len(series[:k])
            break
E=0
for s in allseries:
    E+=len(s)*allseries[s]
print("Average series length", E)

#Guess winners before game
m=0
for i in range(2**7):
    series=str(bin(i)[2:])
    guess=series.zfill(7)
    guess=[int(g) for g in guess]
    correctprob=0
    for s in allseries:
        count=0
        for gi in range(min(7,len(s))):
            if guess[gi]==s[gi]:
                count+=1
        if count>=Threshold:
            correctprob+=allseries[s]
    if correctprob>m:
        m=correctprob
print("Guess winners before game:",m)

#Guess winners during game
def winprob(nguesses,pastguesses,ncorrect):
    if ncorrect>=Threshold:
        return 1
    if sum(pastguesses)>=4 or pastguesses.count(0)>=4:
        if ncorrect>=Threshold:
            return 1
        else:
            return 0
    pastguesses1=pastguesses[:]
    pastguesses0=pastguesses[:]
    pastguesses1.append(1)
    pastguesses0.append(0)
    guess1=.5*winprob(nguesses+1,pastguesses1,ncorrect+1)+.5*winprob(nguesses+1,pastguesses0,ncorrect)
    guess0=.5*winprob(nguesses+1,pastguesses1,ncorrect)+.5*winprob(nguesses+1,pastguesses0,ncorrect+1)
    #print(nguesses,pastguesses,guess0,guess1)
    return max(guess0,guess1)
    
print("Guess winners during game:",winprob(0,[],0))


"""Output:
Average series length 5.8125
Guess winners before game: 0.90625
Guess winners during game: 0.9375
"""
