#Define S(n,k) as the probability of k nematodes on the nth generation.
#We have the recursive equation S(n,k)=summation from i=0 to floor(k/3) of S(n-1,k-i)*P(i,k-i). 
#Where P is the binomial equation given by P(x,N) = (floor(N/2) choose x) * (1/2)^floor(N/2), 
#which is the probability that x new nematodes are spawned

import math
import operator as op
from functools import reduce

#https://stackoverflow.com/questions/4941753/is-there-a-math-ncr-function-in-python
def ncr(n, r):
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer // denom  # or / in Python 2

def P(x,N):
    T=int(math.floor(float(N)/2))
    return ncr(T,x)*0.5**T
    
def S(n,k):
    if n==0 and k==2:
        return 1
    elif n==0:
        return 0
    else:
        imax=int(math.floor(float(k)/3))
        s=0
        for i in range(0,imax+1):
            s+=S(n-1,k-i)*P(i,k-i)
        return s
    
def MaxNematodes(n):
    if n==0:
        return 2
    else:
        return MaxNematodes(n-1)+int(math.floor(MaxNematodes(n-1))/2)

E=0
generations=4
for x in range(2,MaxNematodes(generations)+1):
    E+=x*S(generations,x)
print(E)

"""Output:
4.40625
"""