function winrate = riddler2_21_20(T,pA)
decisionmatrix=zeros(T+1,4*T+1);
probmatrix=zeros(T+1,4*T+1);
for t=T:-1:0
    for s=t*2:-1:-t*2
        if t==T && s>0
            probmatrix(t+1,s+2*t+1)=1;
        elseif t==T && s==0
            probmatrix(t+1,s+2*t+1)=0;
        elseif t==T && s<0
            probmatrix(t+1,s+2*t+1)=0;
        else
            t;
            [s,s+2*T+1];
            choose1=pA*probmatrix(t+2,s+2*T+2)+(1-pA)*probmatrix(t+2,s+2*T);
            choose2=.5*probmatrix(t+2,s+2*T+3)+.5*probmatrix(t+2,s+2*T-1);
            if max(choose1,choose2)==choose2
                probmatrix(t+1,s+2*T+1)=choose2;
                decisionmatrix(t+1,s+2*T+1)=2;
            else
                probmatrix(t+1,s+2*T+1)=choose1;
                decisionmatrix(t+1,s+2*T+1)=1;
            end
        end
    end
end
probmatrix;
decisionmatrix;
winrate = probmatrix(1,2*T+1);
end
