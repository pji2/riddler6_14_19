clc
clear
mindist=100;
minangle=30;maxangle=85;
tol=.005;
for ang=minangle:5:maxangle
    x=ang*pi/180;
    T=.1;
    %trace straight line at angle x until we "touch" the edge of moving
    %circle that starts at (12,0)
    while (T*cos(x)-(12-T))^2+(T*sin(x))^2>36 && T<12
        T=T+tol;
    end
    r=T;
    s=0;
    v=x;
    curvedist=0;
    thetas=[0,v];
    rhos=[0,T];
    dr=r;dv=tol;
    %iterate for our curved path around the moving circle
    %until our slope equals the slope to (12,0), our destination
    %point
    while abs((dr/dv*sin(v)+r*cos(v))/(dr/dv*cos(v)-r*sin(v))-(r*sin(v))/(r*cos(v)-12))>=tol
        x2=v-tol;
        %for new theta, solve for r such that we are 6 ft away from the
        %moving circle
        fun=@(r2) (r2*cos(x2)-(12-T-curvedist))^2+(r2*sin(x2))^2-36;
        R2=fzero(fun,r);
        abs((R2*cos(x2)-(12-T-curvedist))^2+(R2*sin(x2))^2-36);
        s=sqrt((R2-r)^2+(r*tol)^2);
        curvedist=curvedist+s;
        dr=R2-r;
        r=R2;
        v=x2;
        thetas=[thetas v];
        rhos=[rhos r];
    end
    finalleg=sqrt((r*sin(v))^2+(r*cos(v)-12)^2);
    D=T+curvedist+finalleg;
    [ang,T,curvedist,D]
    if D<mindist
        mindist=D;
        Thetas=thetas;Rhos=rhos;
    end
end
mindist
polar([Thetas,0],[Rhos,12])
