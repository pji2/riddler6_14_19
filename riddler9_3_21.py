#simulated annealing adapted from https://bl.ocks.org/ostrowr/b011fc73aab1d54994838cc6c8146847 
import random
import numpy as np
import csv
def genrandom(C):
    strat=[0]*C
    for i in range(1000):
        ind=random.randint(1,C)
        strat[ind-1]+=1
    return np.array(strat)

def randomstrats(size):
    allstrats=[]
    for _ in range(size):
        allstrats.append(genrandom(10))
    return np.array(allstrats)

#>45.5 points to win
def getpoints(a,b):
    greater=np.where((a-b)>0)[0]
    lesser=np.where((a-b)<0)[0]
    equal=np.where((a-b)==0)[0]
    return sum(greater+1)+sum(equal+1)/2.0

def battle(allstrats,strat):
    Record=[]
    for i in range(len(allstrats)):
        wins=0
        b=allstrats[i]
        points=getpoints(strat,b)
        if points>27.5:
            wins=1
        elif points==27.5:
            wins=0.5
        Record.append(wins)
    Record=np.array(Record)
    return np.sum(Record)/Record.shape[0]

def alter(strat):
    n=len(strat)
    for ni in range(random.randint(1,5)):
        i=random.randint(0,n-1)
        j=random.randint(0,n-1)
        while i==j or strat[j]==0:
            i=random.randint(0,n-1)
            j=random.randint(0,n-1)
        strat[i]+=1
        strat[j]-=1
    return strat

def csvread():
    strats=[[100,100,100,100,100,100,100,100,100,100]]
    with open('riddlercastle4strats.csv') as csvfile:
        reader=csv.reader(csvfile)
        r=0
        for row in reader:
            if r==0:
                r+=1
                continue
            l=[int(r) for r in row]
            strats=np.vstack([strats,l])
    return strats

def main():
    allstratsorig=csvread()
    allstrats=csvread()
    N=100
    compete=[2,2,4,124,185,11,34,27,347,264] #agent based modeling
    #compete=[1,2,5,114,190,19,32,32,350,255]
    #compete=[7, 15,  91,  34,  37, 212,  29,  26, 311, 238] k-means
    bestwinrate=battle(allstratsorig,compete)
    print(bestwinrate)
    beststrat=[]
    for i in range(1000):
        oldwinrate=0.5*battle(allstrats,compete)+0.5*battle(allstratsorig,compete)
        compete=alter(compete)
        newwinrate=0.5*battle(allstrats,compete)+0.5*battle(allstratsorig,compete)
        if newwinrate>oldwinrate or random.random()<0.1:
            allstrats=np.vstack([allstrats,compete])
        if newwinrate>bestwinrate:
            beststrat=compete
            bestwinrate=newwinrate
    print(beststrat,bestwinrate)
main()

### example output:
0.8618490967056323
[26, 17, 52, 129, 162, 0, 5, 80, 269, 260] 0.8695646306698379
###