from functools import reduce
from math import sqrt
#https://stackoverflow.com/questions/6800193/what-is-the-most-efficient-way-of-finding-all-the-factors-of-a-number-in-python
def factors(n):
    step = 2 if n%2 else 1
    return list(set(reduce(list.__add__,
            ([i, n//i] for i in range(1, int(sqrt(n))+1, step) if n % i == 0))))

primes=[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997]
for N in range(4,26):
    print("-------",N)
    currentscore=0
    sumprimesgreaterNd2=0
    setprimes=[]
    for p in primes:
        if p<=N:
            start=p
        if p>int(N/2+0.5) and p<=N:
            sumprimesgreaterNd2+=p
            #if p!=N:
            setprimes.append(p)
    t=N*(N+1)/2.0-(int(N/2.0+0.5)*int(N/2.0+1.5))/2.0 #sum of all N greater than N/2
    print(N,"theoretical maximum,",t/(N*(N+1)/2.0)) #(3N+2)/(4N+4)
    numbersavailable=[x for x in range(1,N+1)]
    remainingset=[]
    for n in numbersavailable:
        if n not in setprimes:
            remainingset.append(n)
    #theoretical maximum after choosing first prime, removing all other remainiing primes > N/2, and choosing greatest 1/2
    theoretical=sum(remainingset[int(len(remainingset)/2.0+.5):])+start
    print("theoretical maximum after removing primes,", theoretical/(N*(N+1)/2.0))
    numbersavailable.remove(start)
    numbersavailable.remove(1)
    bestscore=0
    bestchoices=[]
    def numberchoices(numbersavailable, currentscore, pastchoices):
        if len(numbersavailable)==0:
            global bestscore, bestchoices
            if currentscore>bestscore:
                bestscore=currentscore
                bestchoices=pastchoices
        numbersavailable_r=numbersavailable[::-1]
        for n in numbersavailable_r:
            fs=factors(n)
            fs_=[f for f in fs if f in numbersavailable]
            fs_.remove(n)
            numbersavailable_=numbersavailable[:]
            numbersavailable_.remove(n)
            if len(fs_)==0:
                if currentscore>bestscore:
                    bestscore=currentscore
                    bestchoices=pastchoices
            else:
                pastchoices_=pastchoices[:]
                pastchoices_.append(n)
                for f in fs_:
                    numbersavailable_.remove(f)
                numberchoices(numbersavailable_,currentscore+n,pastchoices_)
                    #print(f)
    numberchoices(numbersavailable,start,[start])

    print("actual:",bestscore/sum([x for x in range(1,N+1)]),bestchoices)
maxtheoretical=0
maxN=0
for N in range(26,1001):
    setprimes=[]
    for p in primes:
        if p<=N:
            start=p
        if p>int(N/2+0.5) and p<=N:
            #if p!=N:
            setprimes.append(p)
    t=N*(N+1)/2.0-(int(N/2.0+0.5)*int(N/2.0+1.5))/2.0 #sum of all N greater than N/2
    #print(N,"theoretical maximum,",t/(N*(N+1)/2.0)) #(3N+2)/(4N+4)
    numbersavailable=[x for x in range(1,N+1)]
    remainingset=[]
    for n in numbersavailable:
        if n not in setprimes:
            remainingset.append(n)
    #theoretical maximum after choosing first prime, removing all other remainiing primes > N/2, and choosing greatest 1/2
    theoretical=sum(remainingset[int(len(remainingset)/2.0+.5):])+start
    if theoretical/(N*(N+1)/2.0)>maxtheoretical:
        maxtheoretical=theoretical/(N*(N+1)/2.0)
        maxN=N
    #print("theoretical maximum after removing primes,", theoretical/(N*(N+1)/2.0))
print("maximum theoretical for N>25 up to 1000,",maxN, maxtheoretical)


"""Output:
------- 4
4 theoretical maximum, 0.7
theoretical maximum after removing primes, 0.7
actual: 0.7 [3, 4]
------- 5
5 theoretical maximum, 0.6
theoretical maximum after removing primes, 0.8
actual: 0.6 [5, 4]
------- 6
6 theoretical maximum, 0.7142857142857143
theoretical maximum after removing primes, 0.7142857142857143
actual: 0.7142857142857143 [5, 4, 6]
------- 7
7 theoretical maximum, 0.6428571428571429
theoretical maximum after removing primes, 0.6071428571428571
actual: 0.6071428571428571 [7, 4, 6]
------- 8
8 theoretical maximum, 0.7222222222222222
theoretical maximum after removing primes, 0.6944444444444444
actual: 0.5833333333333334 [7, 8, 6]
------- 9
9 theoretical maximum, 0.6666666666666666
theoretical maximum after removing primes, 0.7777777777777778
actual: 0.6666666666666666 [7, 9, 6, 8]
------- 10
10 theoretical maximum, 0.7272727272727273
theoretical maximum after removing primes, 0.7272727272727273
actual: 0.7272727272727273 [7, 9, 6, 10, 8]
------- 11
11 theoretical maximum, 0.6818181818181818
theoretical maximum after removing primes, 0.6666666666666666
actual: 0.6666666666666666 [11, 9, 6, 10, 8]
------- 12
12 theoretical maximum, 0.7307692307692307
theoretical maximum after removing primes, 0.717948717948718
actual: 0.6410256410256411 [11, 10, 9, 8, 12]
------- 13
13 theoretical maximum, 0.6923076923076923
theoretical maximum after removing primes, 0.6483516483516484
actual: 0.5714285714285714 [13, 10, 9, 8, 12]
------- 14
14 theoretical maximum, 0.7333333333333333
theoretical maximum after removing primes, 0.6952380952380952
actual: 0.6285714285714286 [13, 14, 10, 9, 8, 12]
------- 15
15 theoretical maximum, 0.7
theoretical maximum after removing primes, 0.675
actual: 0.675 [13, 9, 15, 10, 14, 8, 12]
------- 16
16 theoretical maximum, 0.7352941176470589
theoretical maximum after removing primes, 0.7132352941176471
actual: 0.6544117647058824 [13, 9, 15, 10, 16, 14, 12]
------- 17
17 theoretical maximum, 0.7058823529411765
theoretical maximum after removing primes, 0.6601307189542484
actual: 0.6078431372549019 [17, 9, 15, 10, 16, 14, 12]
------- 18
18 theoretical maximum, 0.7368421052631579
theoretical maximum after removing primes, 0.6491228070175439
actual: 0.6491228070175439 [17, 9, 15, 10, 18, 14, 12, 16]
------- 19
19 theoretical maximum, 0.7105263157894737
theoretical maximum after removing primes, 0.5947368421052631
actual: 0.5947368421052631 [19, 9, 15, 10, 18, 14, 12, 16]
------- 20
20 theoretical maximum, 0.7380952380952381
theoretical maximum after removing primes, 0.6333333333333333
actual: 0.5904761904761905 [19, 15, 10, 20, 16, 14, 12, 18]
------- 21
21 theoretical maximum, 0.7142857142857143
theoretical maximum after removing primes, 0.6753246753246753
actual: 0.6233766233766234 [19, 9, 21, 15, 14, 18, 12, 20, 16]
------- 22
22 theoretical maximum, 0.7391304347826086
theoretical maximum after removing primes, 0.6640316205533597
actual: 0.6561264822134387 [19, 9, 21, 15, 14, 22, 18, 12, 20, 16]
------- 23
23 theoretical maximum, 0.717391304347826
theoretical maximum after removing primes, 0.6231884057971014
actual: 0.6159420289855072 [23, 9, 21, 15, 14, 22, 18, 12, 20, 16]
------- 24
24 theoretical maximum, 0.74
theoretical maximum after removing primes, 0.6533333333333333
actual: 0.6066666666666667 [23, 9, 21, 15, 14, 22, 20, 18, 16, 24]
------- 25
25 theoretical maximum, 0.72
theoretical maximum after removing primes, 0.6861538461538461
actual: 0.6092307692307692 [23, 25, 15, 21, 14, 22, 20, 18, 16, 24]
maximum theoretical for N>25 up to 1000, 27 0.6984126984126984
"""