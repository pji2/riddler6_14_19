import random
from itertools import permutations
from itertools import combinations

#function taken from "Kris" https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect?noredirect=1&lq=1
def find_intersection( p0, p1, p2, p3 ) :

    s10_x = p1[0] - p0[0]
    s10_y = p1[1] - p0[1]
    s32_x = p3[0] - p2[0]
    s32_y = p3[1] - p2[1]

    denom = s10_x * s32_y - s32_x * s10_y

    if denom == 0 : return False # collinear

    denom_is_positive = denom > 0

    s02_x = p0[0] - p2[0]
    s02_y = p0[1] - p2[1]

    s_numer = s10_x * s02_y - s10_y * s02_x

    if (s_numer < 0) == denom_is_positive : return False # no collision

    t_numer = s32_x * s02_y - s32_y * s02_x

    if (t_numer < 0) == denom_is_positive : return False # no collision

    if (s_numer > denom) == denom_is_positive or (t_numer > denom) == denom_is_positive : return False # no collision


    # collision detected

    t = t_numer / denom

    ip = ( p0[0] + (t * s10_x), p0[1] + (t * s10_y) )

    #exclude case where line segments intersect at endpoints
    if ip==p0 or ip==p1 or ip==p2 or ip==p3:
        return False
    
    #print(ip,p0,p1,p2,p3)
    return True
    
def genpoints(N):
    points=[]
    while len(points)<N:
        point=(float(round(random.random()*10000)/1000.0),float(round(random.random()*10000)/1000.0)) #need to avoid colinear points
        if point not in points:
            points.append(point)
    return points

def validpolygon(order,points,N):
    selfcrossing=False
    edges=[]
    for i in range(N):
        if i==0:
            edges.append([0,order[i]])
        elif i==N-1:
            edges.append([order[N-2],0])
        else:
            edges.append([order[i-1],order[i]])
    #print(order,edges)
    for c in list(combinations(range(N),2)):
        edge1=edges[c[0]]
        edge2=edges[c[1]]
        p1=points[edge1[0]]
        q1=points[edge1[1]]
        p2=points[edge2[0]]
        q2=points[edge2[1]]
        #print(c,p1,q1,p2,q2,find_intersection(p1,q1,p2,q2))
        if find_intersection(p1,q1,p2,q2):
            return False
    ##print(order,edges)
    return True
        
def main():
    maxpolys=0
    maxpoints=[]
    for n in range(100000):
        N=6
        validpolys=0
        points=genpoints(N)
        ##print(points)
        orderings=[]
        for order in list(permutations(range(1,N))):
            if order[0]<order[-1]:
                orderings.append(order)
        for order in orderings:
            if validpolygon(order,points,N):
                validpolys+=1
        if validpolys>maxpolys:
            maxpolys=validpolys
            maxpoints=points
    print(maxpolys)
    print(maxpoints)
            
main() #29