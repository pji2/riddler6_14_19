def gamestate3(turn, num):
    if num==0:
        return (turn+2)%3,[turn,(turn+1)%3]
    winners=[]
    eliminationhist=[]
    if num==20:
        return gamestate3((turn+1)%3,num-1)
    for moves in range(1,5):
        if num-moves>=0:
            res=gamestate3((turn+1)%3,num-moves)
            winners.append(res[0])
            eliminationhist.append(res[1])
    if turn in winners:
        i=winners.index(turn)
        return turn, eliminationhist[i]
    else:
        lastelim_ind=0
        choice=0
        for i in range(len(eliminationhist)):
            his=eliminationhist[i]
            ind=his.index(turn)
            if ind>lastelim_ind:
                lastelim_ind=ind
                choice=i
        return winners[choice],eliminationhist[choice]
    
def gamestate4(turn, num):
    if num==0:
        return (turn+2)%4,[turn,(turn+3)%4,(turn+1)%4]
    winners=[]
    eliminationhist=[]
    if num==20:
        return gamestate4((turn+1)%4,num-1)
    for moves in range(1,5):
        if num-moves>=0:
            res=gamestate4((turn+1)%4,num-moves)
            winners.append(res[0])
            eliminationhist.append(res[1])
    if turn in winners:
        i=winners.index(turn)
        return turn, eliminationhist[i]
    else:
        lastelim_ind=0
        choice=0
        for i in range(len(eliminationhist)):
            his=eliminationhist[i]
            ind=his.index(turn)
            if ind>lastelim_ind:
                lastelim_ind=ind
                choice=i
        return winners[choice],eliminationhist[choice]
    
print(gamestate3(0,20))
print(gamestate4(0,20))

"""Output:
(Winning player, elimination order) Player A=0
(1, [2, 0])
(2, [0, 3, 1])
"""