land_areas=dict()
with open('state_areas.csv') as fo:
    l=fo.readline()
    l=l.split(",")
    while l and l[0]!="":
        land_areas[l[0]]=l[4]
        l=fo.readline()
        l=l.split(",")
print(land_areas)

us_state_connectivity=dict()
#https://writeonly.wordpress.com/2009/03/20/adjacency-list-of-states-of-the-united-states-us/
with open('us_states_adjacency.txt') as fl:
    l=fl.readline()
    while l:
        l=l[:-1]
        states=l.split(",")
        us_state_connectivity[states[0]]=list(states[1:])
        l=fl.readline()
print(us_state_connectivity)
borderstates=['WA','OR','CA','AZ','NM','TX','LA','MS','AL','FL','GA','SC','NC',\
             'VA','MD','DE','NJ','NY','CT','RI','MA','NH','ME','VT','PA','OH',\
             'MILO','IN','IL','WI','MIUP','MN','ND','MT','ID']

#don't pick a new state to remove that is adjacent to two states already removed
def notadjacent(state,removed,connectivity):
    num=0
    for s in removed:
        adjs=connectivity[state]
        if s in adjs:
            num+=1
    if num==0:
        print("error")
        return False
    if num==1:
        return True
    return False

def twoborderstates(removed,borderstates):
    numborderstates=0
    for s in removed:
        if s in borderstates:
            numborderstates+=1
    if len(removed)==numborderstates: #they can't all be border states
        return False
    if numborderstates==2:
        return True
    return False

#https://www.geeksforgeeks.org/breadth-first-search-or-bfs-for-a-graph/
# Python3 Program to print BFS traversal
# from a given source vertex. BFS(int s)
# traverses vertices reachable from s.
from collections import defaultdict
 
# This class represents a directed graph
# using adjacency list representation
class Graph:
 
    # Constructor
    def __init__(self):
 
        # default dictionary to store graph
        self.graph = defaultdict(list)
        self.nodes=[]
 
    # function to add an edge to graph
    def addEdge(self,u,v):
        self.graph[u].append(v)
 
    # Function to print a BFS of graph
    def BFS(self, s):
 
        # Mark all the vertices as not visited
        visited = dict()
        for s in self.graph:
            visited[s]=False
 
        # Create a queue for BFS
        queue = []
 
        # Mark the source node as
        # visited and enqueue it
        queue.append(s)
        visited[s] = True
 
        while queue:
 
            # Dequeue a vertex from
            # queue and print it
            s = queue.pop(0)
            self.nodes.append(s)
            # Get all adjacent vertices of the
            # dequeued vertex s. If a adjacent
            # has not been visited, then mark it
            # visited and enqueue it
            for i in self.graph[s]:
                if visited[i] == False:
                    queue.append(i)
                    visited[i] = True

import csv
outputf=open('state_areas_output.csv','w')
csvwriter=csv.writer(outputf)

lower48area=0
for s in us_state_connectivity:
    lower48area+=int(land_areas[s])
print("lower48area",lower48area)    

def getareas(removed,half,otherhalf):
    A=0
    B=0
    for s in half:
        A+=int(land_areas[s])
    for s in otherhalf:
        B+=int(land_areas[s])
    assert(len(half)+len(otherhalf)+len(removed)==49)
    csvwriter.writerow([removed,half,otherhalf,A,B,min(A,B),min(A,B)/float(lower48area)])
    
def analyzepartition(removed,us_state_connectivity):
    g=Graph()
    start_state=""
    for state in us_state_connectivity:
        if state not in removed:
            adjacentstates=us_state_connectivity[state]
            for ad_s in adjacentstates:
                if ad_s not in removed:
                    start_state=state
                    g.addEdge(state,ad_s)
                    g.addEdge(ad_s,state)
    g.BFS(start_state)
    #print("corresponding half",g.nodes, len(g.nodes))
    #states in other half
    otherhalf=[]
    for state in us_state_connectivity:
        if state not in removed and state not in g.nodes:
            otherhalf.append(state)
    getareas(removed,g.nodes,otherhalf)
    #print("other half",otherhalf)

    
def getremovedstates(borderstates,us_state_connectivity,removed):
    #print(removed)
    if len(removed)==0:
        for b in borderstates:
            removed.append(b)
            getremovedstates(borderstates,us_state_connectivity,removed)
            removed.pop()
    if len(removed)>1 and removed[-1] in borderstates and twoborderstates(removed,borderstates):
        analyzepartition(removed,us_state_connectivity)
        return
    if len(removed)>0:
        bordering=us_state_connectivity[removed[-1]]
        for state in bordering:
            if state not in removed and notadjacent(state,removed,us_state_connectivity):
                removed.append(state)
                getremovedstates(borderstates,us_state_connectivity,removed)
                removed.pop()

getremovedstates(borderstates,us_state_connectivity,[])
outputf.close()