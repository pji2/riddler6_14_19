def coingame(player,position):
    nextpositions=[]
    if player==0: #Me
        if position in [0,1,2]:
            return 0 
        if position in [3,4]:
            return 1
        for coinstaken in [1,2]:
            win_lose_state=coingame((player+1)%2,position-coinstaken)
            nextpositions.append(win_lose_state)
    else: #opponent
        if position in [0,2]:
            return 0
        if position in [3,4,5]:
            return 1
        for coinstaken in [2,3]:
            win_lose_state=coingame((player+1)%2,position-coinstaken)
            nextpositions.append(win_lose_state)
    #print(player,position,nextpositions)
    if 0 in nextpositions:
        return 1
    if sum(nextpositions)==len(nextpositions):
        return 0
player=1
print("Player:",player)
for init in range(3,20):
    print("coins:",init,"win/lose:",coingame(player,init))