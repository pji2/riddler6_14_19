points=[[400,350,300],[300,225,200],[250,225,175]]
pickorder="ABCCBAABC"
picked=[["" for x in range(3)] for y in range(3)]


#get the choices available for a given player
def getchoices(picks,player):
    choices=[]
    for i in range(3):
        for j in range(3):
            if player not in picks[i] and picks[i][j]=="":
                choices.append((i,j))
                break #always pick the highest available player in a given position
    return choices

#get the score of a completed board
def getscore(picks):
    scores=[0,0,0]
    for i in range(3):
        for j in range(3):
            if picks[i][j]=='A':
                scores[0]+=points[i][j]
            elif picks[i][j]=='B':
                scores[1]+=points[i][j]
            elif picks[i][j]=='C':
                scores[2]+=points[i][j]
    return scores

#get all maxes of an array
def getmaxes(arr):
    m=max(arr)
    return [i for i, j in enumerate(arr) if j == m]

#deep copy an array
def copy(arr):
    newarr=[["" for x in range(3)] for y in range(3)]
    for i in range(3):
        for j in range(3):
            newarr[i][j]=arr[i][j]
    return newarr

#all possible ways to fill the board with N turns
res=[]
def generatepossiblefillings(picks,N,n,res):
    if n==N:
        p=copy(picks)
        #print(p)
        res.append(p)
        return
    nextplayer=pickorder[n]
    for i in range(3):
        if nextplayer in picks[i]:
            continue
        j=picks[i].index("")
        picks[i][j]=nextplayer
        generatepossiblefillings(picks,N,n+1,res)
        picks[i][j]=""


strats=dict()
#possible strategies for last player
res=[]
scoredict={'A':0,'B':1,'C':2}
generatepossiblefillings(picked,8,0,res)
for r in res:
    turn=pickorder[8]
    choices=getchoices(r,turn)
    scores=[]
    for c in choices:
        r[c[0]][c[1]]=turn
        scor=getscore(r)
        scores.append(scor[scoredict[turn]])
        r[c[0]][c[1]]=""
    maxinds=getmaxes(scores)
    scoreinds=[choices[i] for i in maxinds]
    strats[tuple(map(tuple, r))]=[scoreinds]

#possible strategies for remaining players, iteratively
for player in range(7,-1,-1):
    res=[]
    picked=[["" for x in range(3)] for y in range(3)]
    generatepossiblefillings(picked,player,0,res)
    for r in res:
        turn=pickorder[player]
        choices=getchoices(r,turn)
        scores=[]
        #for each choice available to our player, the following players
        #choose the strategies that maximize their scores
        for c in choices:
            r_c=copy(r)
            r_c[c[0]][c[1]]=turn
            optimalmoves=strats[tuple(map(tuple, r_c))]
            p=player
            for o in optimalmoves[0]:
                p+=1
                r_c[o[0]][o[1]]=pickorder[p]
            scor=getscore(r_c)
            scores.append(scor[scoredict[turn]])
        #find the top choice for our player
        maxinds=getmaxes(scores)
        scoreinds=[choices[i] for i in maxinds]
        r_c=copy(r)
        strats[tuple(map(tuple, r_c))]=[]
        p=player
        #fill the choices into our dict of strategies
        for sc in scoreinds:
            r_c[sc[0]][sc[1]]=pickorder[p]
            optimalmoves=strats[tuple(map(tuple, r_c))]
            for o in optimalmoves:
                s=[sc]
                s.extend(o)
                strats[tuple(map(tuple,r))].append(s)
                r_c=copy(r)

#print the top strategies
points=[[400,350,300],[300,225,200],[250,225,175]]
picked=[["" for x in range(3)] for y in range(3)]
optimalstrats=strats[tuple(map(tuple,picked))]
for i in range(len(optimalstrats)):
    o=optimalstrats[i]
    picks=picked[:]
    for oi in range(len(o)):
        ind=o[oi]
        picks[ind[0]][ind[1]]=pickorder[oi]
    scores=getscore(picks)
    print(picks,scores)

"""Output:
[['A', 'C', 'B'], ['B', 'A', 'C'], ['C', 'B', 'A']] [800, 825, 800]
[['A', 'C', 'B'], ['B', 'A', 'C'], ['C', 'B', 'A']] [800, 825, 800]
[['A', 'C', 'B'], ['B', 'A', 'C'], ['C', 'B', 'A']] [800, 825, 800]
[['A', 'C', 'B'], ['B', 'A', 'C'], ['C', 'B', 'A']] [800, 825, 800]
"""